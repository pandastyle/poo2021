import java.util.HashMap;
import java.util.Map;

public class Banco {
	private String nombre;
	private HashMap<String, Double> cuentas;
	
	public Banco() {
		this("", new HashMap<String,Double>());
	}
	public Banco(String nombre, HashMap<String, Double> cuentas) {
		
		this.nombre = nombre;
		this.cuentas = cuentas;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public HashMap<String, Double> getCuentas() {
		return cuentas;
	}
	public void setCuentas(HashMap<String, Double> cuentas) {
		this.cuentas = cuentas;
	}
	
	public Boolean agregarCuenta(Persona persona, Double saldo) {
		if (cuentas.containsKey(persona.getNombre())) {
			return false;
		}else {
		cuentas.put(persona.getNombre(), saldo);
		return true;
		}
	}
	
	public Boolean eliminarCuenta(Persona persona) {
		this.getCuentas().remove(persona.getNombre());
		return true;
	}
	
	public void listarClientes() {
		Persona perCuenta= new Persona();
		Double perSaldo;
		for (Map.Entry<String, Double> entrada : this.cuentas.entrySet()) {//entrada dentro del hashmap
			perCuenta.setNombre(entrada.getKey());//obtiene el objeto de la key
			perSaldo= entrada.getValue();//obtiene el objeto de valor
			System.out.println("Nombre Cliente: "+ perCuenta.getNombre()+" Saldo: "+perSaldo);
		}
	}
	
	public Double getSaldo(Persona persona) {
		Persona perCuenta= new Persona();
		
		if (cuentas.containsKey(persona.getNombre())) {//verifica si existe dentro del hashmap

		for (Map.Entry<String, Double> entrada : this.cuentas.entrySet()) {//entrada dentro del hashmap
			perCuenta.setNombre(entrada.getKey());
			if (perCuenta.getNombre().equalsIgnoreCase(persona.getNombre())) {
				return entrada.getValue();//obtiene el objeto de valor
			}
		}
		}
		return -1.0;
	}
}

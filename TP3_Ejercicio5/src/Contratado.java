import java.time.LocalDate;

public class Contratado extends Empleado {
	
	private Double tarifaHora;
	private Double cantidadHorasMes;
	
	public Contratado(Integer nroLegajo, String nombre, String puesto, String direccion, String telefono,
			LocalDate fechaNacimiento, LocalDate fechaContratacion, Double tarifaHora,Double cantidadHorasMes) {
		super(nroLegajo, nombre, puesto, direccion, telefono, fechaNacimiento, fechaContratacion);
		this.setTarifaHora(tarifaHora);
		this.setCantidadHorasMes(cantidadHorasMes);
		
	}

	public Double getTarifaHora() {
		return tarifaHora;
	}

	public void setTarifaHora(Double tarifaHora) {
		this.tarifaHora = tarifaHora;
	}

	public Double getCantidadHorasMes() {
		return cantidadHorasMes;
	}

	public void setCantidadHorasMes(Double cantidadHorasMes) {
		this.cantidadHorasMes = cantidadHorasMes;
	}
	
	public Double calcularSueldo() {
		return getCantidadHorasMes()*getTarifaHora();
	}
	
	public String toString() {
		return "Nombre Contratado: "+ getNombre() +" Sueldo:"+ calcularSueldo();
	}
	

}

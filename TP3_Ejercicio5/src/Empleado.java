import java.time.LocalDate;

public class Empleado {
	private Integer nroLegajo;
	private String nombre;
	private String puesto;
	private String direccion;
	private String telefono;
	private LocalDate fechaNacimiento;
	private LocalDate fechaContratacion;
	
	
	
	
	public Empleado(Integer nroLegajo, String nombre, String puesto, String direccion, String telefono,
			LocalDate fechaNacimiento, LocalDate fechaContratacion) {
		super();
		this.nroLegajo = nroLegajo;
		this.nombre = nombre;
		this.puesto = puesto;
		this.direccion = direccion;
		this.telefono = telefono;
		this.fechaNacimiento = fechaNacimiento;
		this.fechaContratacion = fechaContratacion;
	}
	
	public Integer getNroLegajo() {
		return nroLegajo;
	}
	public void setNroLegajo(Integer nroLegajo) {
		this.nroLegajo = nroLegajo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getPuesto() {
		return puesto;
	}
	public void setPuesto(String puesto) {
		this.puesto = puesto;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public LocalDate getFechaContratacion() {
		return fechaContratacion;
	}
	public void setFechaContratacion(LocalDate fechaContratacion) {
		this.fechaContratacion = fechaContratacion;
	}

	@Override
	public String toString() {
		return "Empleado [nroLegajo=" + nroLegajo + ", nombre=" + nombre + ", puesto=" + puesto + ", direccion="
				+ direccion + ", telefono=" + telefono + ", fechaNacimiento=" + fechaNacimiento + ", fechaContratacion="
				+ fechaContratacion + "]";
	}
	
	
	
}

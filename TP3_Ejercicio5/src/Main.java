import java.time.LocalDate;
import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		ArrayList<Empleado> empleados= new ArrayList<Empleado>();
		Trabajador trabajador= new Trabajador(2050, "Dylan Salazar", "Programador", "Diadema Argentina", "2974337349",
											  LocalDate.of(1999, 11, 27), LocalDate.of(2020, 9, 5),
											  10000.0, 500.0, 2500.0, 900.0);
		Contratado contratado= new Contratado(1020, "Juan Pablo", "Dise�ador", "Comodoro Rivadavia", 
				  							 "2974239578", LocalDate.of(1997, 7, 5), LocalDate.of(2015, 2, 21), 300.0, 72.0);
		
		empleados.add(contratado);
		empleados.add(trabajador);
		
		for (Empleado empleado : empleados) {
			System.out.println(empleado.toString());
		}
		
	}

}

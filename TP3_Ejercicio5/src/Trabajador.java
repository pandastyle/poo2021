import java.time.LocalDate;

public class Trabajador extends Empleado {
	private Double sueldo;
	private Double retenciones;
	private Double impuestos;
	private Double premios;
	
	
	public Trabajador(Integer nroLegajo, String nombre, String puesto, String direccion, String telefono,
			LocalDate fechaNacimiento, LocalDate fechaContratacion, Double sueldo, Double retenciones, Double impuestos,
			Double premios) {
		
		super(nroLegajo, nombre, puesto, direccion, telefono, fechaNacimiento, fechaContratacion);
		this.setSueldo(sueldo);
		this.setRetenciones(retenciones);
		this.setImpuestos(impuestos);
		this.setPremios(premios);
		
		
	}


	public Double getSueldo() {
		return sueldo;
	}


	public void setSueldo(Double sueldo) {
		this.sueldo = sueldo;
	}


	public Double getRetenciones() {
		return retenciones;
	}


	public void setRetenciones(Double retenciones) {
		this.retenciones = retenciones;
	}


	public Double getImpuestos() {
		return impuestos;
	}


	public void setImpuestos(Double impuestos) {
		this.impuestos = impuestos;
	}


	public Double getPremios() {
		return premios;
	}


	public void setPremios(Double premios) {
		this.premios = premios;
	}
	
	public Double calcularSueldo() {
		return (getSueldo()-getRetenciones()-getImpuestos())+getPremios();
	}


	@Override
	public String toString() {
		return "Nombre Trabajador: "+ getNombre() +" Sueldo:"+ calcularSueldo();
	}
	
	
	

}

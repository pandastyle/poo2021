import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class EjemploPath {

	public static void main(String[] args) throws IOException {
		Scanner consola = new Scanner(System.in);
		System.out.println("ingrese nombre del archivo o directorio:  ");
		Path path = Paths.get(consola.nextLine());
		
		if (Files.exists(path)) {//verificar si existe
			System.out.printf("%n%s existe%n",path.getFileName());//%n nueva linea
			//path.getFileName() da nombre del archivo
			System.out.printf("%s un directorio%n", Files.isDirectory(path)? "Es": "No es");//if corto
			System.out.printf("%s es una direccion absoluta%n",path.isAbsolute()?"Es": "No es");
			System.out.printf("Ultima modificacion: %s%n", Files.getLastModifiedTime(path));
			System.out.printf("Tama�o %s%n", Files.size(path));
			System.out.printf("Direccion: %s%n",path);
			System.out.printf("Direccion absoluta: %s%n", path.toAbsolutePath());
			
			if (Files.isDirectory(path)) {
				System.out.printf("%nContenido: %n");
				DirectoryStream<Path> directoryStream = Files.newDirectoryStream(path);
				
				for (Path p: directoryStream) {
					System.out.println(p);
				}//for each	
			}//if
			else {
				System.out.printf("%s no existe%n", path);
			}
		}
		

	}

}

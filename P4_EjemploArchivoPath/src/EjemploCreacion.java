import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class EjemploCreacion {

	public static void main(String[] args) throws FileNotFoundException {
		try(Formatter output = new Formatter("assets/clients.txt")){
			Scanner consola = new Scanner(System.in);	
			System.out.printf("%s%n%s",
					"Ingrese numero de cuenta, nombre, apellido, balance.",
					"Ctrl+z para Finalizar");
			System.out.println("");
			
            while(consola.hasNext()) {
            	try {
            	output.format("%d %s %s %.2f %n", 
            			consola.nextInt(), 
            			consola.next(),
            			consola.next(),
            			consola.nextDouble()); 
            	//en vez  de consola.next se le pone directo el objeto
            	}
            	catch(NoSuchElementException elementException) {
            		System.err.println("valor invalido, ingrese otra vez");
            		consola.nextLine();
            	}//catch
            	System.out.print("? ");
            }//end while
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

}

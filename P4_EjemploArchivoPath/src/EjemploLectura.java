import java.io.IOException;
import java.nio.file.Paths;
import java.util.Scanner;

public class EjemploLectura {
public static void main(String[] args) {
	try(Scanner consola= new Scanner(Paths.get("assets/clients.txt"))){
		System.out.printf("%-10s%-12s%-12s%10s%n","cuenta","nombre","apellido","balance");
		
		while (consola.hasNext()) {
		System.out.printf("%-10d%-12s%-12s%10.2f%n",
				consola.nextInt(),consola.next(),consola.next(),consola.nextDouble());
			
		}		
	} catch (IOException e) {//exception
		
		e.printStackTrace();
	}
}
}

package controlador;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import vista.Vista;

public class Controlador implements MouseListener,MouseMotionListener {
	private Vista vista;
	
	public Controlador() {
		this.vista= new Vista(this);
	    this.vista.setVisible(true);
	}
	

	private void eventOutPut(String evento, MouseEvent e) {
		getVista().getTextArea().append(evento + " ("+ e.getX() + ","+ e.getY()+")"+
	 
				"Detectado en " + e.getComponent().getClass().getName()
				+"\n");
		getVista().getTextArea().setCaretPosition(getVista().getTextArea().getDocument().getLength());
		
	}


	@Override
	public void mouseDragged(MouseEvent e) {
		eventOutPut("Mouse Dragged", e);
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		eventOutPut("Mouse Moved", e);
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		eventOutPut("Mouse Clicked (# de clicks: "+ e.getClickCount()+" )", e);//cantidad de clicks
		
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		eventOutPut("Mouse Pressed", e);
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		eventOutPut("Mouse Released (# de clicks: "+ e.getClickCount()+" )", e);
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		eventOutPut("Mouse Entered", e);
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		eventOutPut("Mouse Exited", e);
		
	}

	public Vista getVista() {
		return vista;
	}

}

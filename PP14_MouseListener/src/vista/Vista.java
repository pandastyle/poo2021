package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.Controlador;

import java.awt.SystemColor;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class Vista extends JFrame {

	private JPanel contentPane;
	private JTextArea textArea;
	private JPanel panel;
	private Controlador controlador;

	/**
	 * Launch the application.
	 */
	

	/**
	 * Create the frame.
	 */
	public Vista(Controlador c) {
		setControlador(c);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 488, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		panel = new JPanel();
		panel.addMouseListener(getControlador()); //asocia el mouse listener con el controlador
		panel.addMouseMotionListener(getControlador());
		panel.setBackground(SystemColor.activeCaption);
		panel.setBounds(10, 11, 452, 158);
		contentPane.add(panel);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 179, 452, 171);
		contentPane.add(scrollPane);
		
		textArea = new JTextArea();
		textArea.addMouseListener(getControlador()); //asocia el scrollpane con el controlador
		textArea.addMouseMotionListener(getControlador());
		scrollPane.setViewportView(textArea);
		
		//asociando el frame/vista
		addMouseListener(getControlador()); 
		addMouseMotionListener(getControlador());
	}

	public JTextArea getTextArea() {
		return textArea;
	}

	public void setTextArea(JTextArea textArea) {
		this.textArea = textArea;
	}

	public JPanel getPanel() {
		return panel;
	}

	public void setPanel(JPanel panel) {
		this.panel = panel;
	}

	public Controlador getControlador() {
		return controlador;
	}

	public void setControlador(Controlador controlador) {
		this.controlador = controlador;
	}
}

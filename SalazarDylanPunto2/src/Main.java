import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		Teclado teclado= new Teclado("Ganon XS QWERTY","teclado","NogaNet");
		Auricular auri= new Auricular("Cloud Alpha S blackout","auricular","HyperX");
		Parlante parlante= new Parlante("SP-Q160","parlante","Genius");
		
		
		System.out.println(auri.reproducirSonido());
		System.out.println(parlante.reproducirSonido());
		System.out.println(teclado.teclearMensaje());
		
		ArrayList<Periferico> perifericos= new ArrayList<Periferico>();

		perifericos.add(teclado);
		perifericos.add(auri);
		perifericos.add(parlante);
		
		System.out.println("------------------------");
		for (Periferico periferico : perifericos) {
			System.out.println(periferico.mostrarInfo());
			System.out.println(periferico.conectar()+" \n");
		}

	}

}


public abstract class Periferico {
	private String nombre;
	private String tipo_periferico;
	private String marca;
	
	public Periferico(String nombre, String tipo_periferico, String marca) {
		
		this.setNombre(nombre);
		this.setTipo_periferico(tipo_periferico);
		this.setMarca(marca);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTipo_periferico() {
		return tipo_periferico;
	}

	public void setTipo_periferico(String tipo_periferico) {
		this.tipo_periferico = tipo_periferico;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}
	
	public String conectar() {
		return "periferico conectado por USB";
	}
	
	public String mostrarInfo() {
		return "Nombre: "+getNombre()+" Tipo Periferico: "+ getTipo_periferico()+ " Marca:"+ getMarca();
	}
	
	
	
	
}

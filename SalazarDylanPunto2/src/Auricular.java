
public class Auricular extends Periferico implements Reproducir {

	public Auricular(String nombre, String tipo_periferico, String marca) {
		super(nombre, tipo_periferico, marca);
		
	}
	
	public String conectar() {
		return "periferico conectado por Auxiliar";
	}

	@Override
	public String reproducirSonido() {
		
		return "Reproduciendo sonido por Auricular "+ getNombre();
	}

}


public class Parlante extends Periferico implements Reproducir {

	public Parlante(String nombre, String tipo_periferico, String marca) {
		super(nombre, tipo_periferico, marca);
		
	}

	@Override
	public String reproducirSonido() {
		
		return "Reproduciendo sonido por Parlante "+ getNombre();
	}

}

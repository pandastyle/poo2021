import java.time.LocalDate;
import java.util.Random;

public class Persona {
	
	private String nombre;
	private String genero;
	private String email;
	private String contrasena;
	private LocalDate fechaNacimiento;
	
	public Persona() {
		
	}

	public Persona(String nombre, String genero, String email,LocalDate fechaNacimiento) {
		super();
		this.setNombre(nombre);
		this.setGenero(genero);
		this.setEmail(email);
		this.setFechaNacimiento(fechaNacimiento);
		this.setContrasena(this.autogenerar());
	
	}
	
	public Persona(String nombre, String genero, String email,String contrasena,LocalDate fechaNacimiento) {
		super();
		this.setNombre(nombre);
		this.setGenero(genero);
		this.setEmail(email);
		this.setFechaNacimiento(fechaNacimiento);
		this.setContrasena(contrasena);
	
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	@Override
	public String toString() {
		return "Persona [nombre=" + nombre + ", genero=" + genero + ", email=" + email + ", contrasena=" + contrasena
				+ ", fechaNacimiento=" + fechaNacimiento + "]";
	}
	
	
	
	private String autogenerar() {
		Random rnd= new Random();
		Integer num = rnd.nextInt(100)+1;
		return "pas"+num;
	}
	
}

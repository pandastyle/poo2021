import java.time.LocalDate;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
        Persona persona = new Persona("Alejandro", "M", "ale@gmail.com", LocalDate.of(1988, 05, 11));
        System.out.println(persona.toString());
        
        Persona persona2 = new Persona("Alejandro", "M", "ale@gmail.com","agregopassword", LocalDate.of(1988, 05, 11));
        System.out.println(persona2.toString());
	}

}

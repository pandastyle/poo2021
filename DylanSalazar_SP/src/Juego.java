import java.util.LinkedList;

public class Juego extends Thread {
	private Servidor servidor;
	private LinkedList<Jugador> equipo = new LinkedList<Jugador>();
	
	
	public Juego(Servidor servidor) {
		super();
		this.servidor = servidor;
	}
	
	public void run() {
		Integer contador=0;
		while (contador<2) {
			Jugador jugador= this.getServidor().consultar();
			if (jugador!=null) {
				this.getEquipo().add(jugador);
			} else {
				contador++;
			}
		}
		this.armarPartida();
		System.out.println("Termino Partida");
		
	}
	public void armarPartida() {
		//separar de a pares //ganador ingresa//se usa siempre la misma lista
		while (!this.getEquipo().isEmpty() && this.getEquipo().size()!=1) {
			Jugador jug1= this.getEquipo().removeFirst();
			Jugador jug2= this.getEquipo().removeFirst();
			this.getEquipo().add(this.enfrentarse(jug1, jug2));	
		}
		System.out.println("EL GANADOR DE LA PARTIDA ES: "+ this.getEquipo().removeFirst().toString());
	}
	
	public Jugador enfrentarse(Jugador jug1, Jugador jug2) {
    System.out.println("ENFRENTAMIENTO: "+jug1.getUsuario()+" VS "+jug2.getUsuario());
    if (jug1.precisionTiro()>jug2.precisionTiro()) {
		System.out.println("GANADOR: "+ jug1.getUsuario());
		return jug1;
	}else {
		System.out.println("GANADOR: "+ jug2.getUsuario());
		return jug2;
	}
	}
	
	public Servidor getServidor() {
		return servidor;
	}
	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}
	public LinkedList<Jugador> getEquipo() {
		return equipo;
	}
	public void setEquipo(LinkedList<Jugador> equipo) {
		this.equipo = equipo;
	}
	
	

}

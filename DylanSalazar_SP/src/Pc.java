import java.util.Random;

public class Pc extends Thread implements Conexion {
	private Servidor servidor;

	public Pc(Servidor servidor) {
		super();
		this.servidor = servidor;
	}

	public void realizarConexion(String nombre) throws FalloConexion {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String pais="";
		Integer valorPais= new Random().nextInt(3);
		
		switch (valorPais) {
		case 0:
			pais="Chile";
			break;
			
		case 1:
			pais="Argentina";
			break;
			
		case 2:
			pais="Brasil";
		break;

		default:
			break;
		}
		Jugador jugadorpc= new JugadorPc(nombre, pais);
		if (new Random().nextInt(100)<25) {
			throw new FalloConexion("FALLO: El "+nombre+", intento conectarse sin �xito mediante una Pc");
		}
		
		this.getServidor().sala(jugadorpc);
	}
	
	public void run() {
		for (int i = 0; i < 5; i++) {
			try {
				this.realizarConexion("jugadorPc"+i);
			} catch (FalloConexion e) {
				System.err.println(e);
			}
		}
		this.getServidor().sala(null);
	}
	
	public Servidor getServidor() {
		return servidor;
	}

	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}
	
	
}

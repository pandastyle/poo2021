import java.util.Random;

public abstract class Jugador {
	private String usuario;
	private Integer nivel;
	private String pais;
	
	public Jugador(String usuario, String pais) {
		super();
		this.usuario = usuario;
		this.pais = pais;
		this.setNivel(new Random().nextInt(80)+1);
	}
	
	

	@Override
	public String toString() {
		return "Jugador [usuario=" + usuario + ", nivel=" + nivel + ", pais=" + pais + "]";
	}
	
	public Integer precisionTiro() {
		return (this.getNivel()*new Random().nextInt(3)+1);
	}


	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public Integer getNivel() {
		return nivel;
	}

	public void setNivel(Integer nivel) {
		this.nivel = nivel;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}
	
	
	
	
}

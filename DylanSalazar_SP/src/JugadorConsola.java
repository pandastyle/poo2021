
public class JugadorConsola extends Jugador {

	public JugadorConsola(String usuario, String pais) {
		super(usuario, pais);
		
	}

	public Integer precisionTiro() {
		Integer precision=super.precisionTiro();
		Integer aimbot=0;
		if (this.getNivel()<20) {
			aimbot = aimbot + 100;
		}
		if(this.getNivel()>=20 && this.getNivel()<=60) {
			aimbot = aimbot + 60;
		}
		if (this.getNivel()>60) {
			aimbot = aimbot + 30;
		}
		return precision*aimbot ;
	}
}

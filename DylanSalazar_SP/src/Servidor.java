import java.util.LinkedList;

public class Servidor {
	private LinkedList<Jugador> jugadores = new LinkedList<Jugador>();
	
	public synchronized void sala(Jugador jugador) {
		while(this.getJugadores().size()>=5) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		if (jugador!=null) {
			System.out.println("El jugador "+jugador.getUsuario()+", ha ingresado en la sala");
		}
		this.getJugadores().add(jugador);
		notifyAll();
	}
	
	public synchronized Jugador consultar() {
		while (this.getJugadores().isEmpty()) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		Jugador jug= this.getJugadores().removeFirst();
		notifyAll();
		return jug;
	}

	public LinkedList<Jugador> getJugadores() {
		return jugadores;
	}

	public void setJugadores(LinkedList<Jugador> jugadores) {
		this.jugadores = jugadores;
	}
	
	
}

import java.util.Random;

public class Consola extends Thread implements Conexion {
	private Servidor servidor;
	
	public Consola(Servidor servidor) {
		super();
		this.servidor = servidor;
	}
	
	public void run() {
		for (int i = 0; i < 5; i++) {
			try {
				this.realizarConexion("jugadorConsola"+i);
			} catch (FalloConexion e) {
				System.err.println(e);
			}
		}
		
		this.getServidor().sala(null);
	}
	
	@Override
	public void realizarConexion(String nombre) throws FalloConexion {
		String pais="";
		
		if (new Random().nextBoolean()){
			pais= "Bolivia";
		}else {
			pais= "Uruguay";
		}
		
		Jugador jugadorconsola= new JugadorConsola(nombre,pais);
		if (new Random().nextInt(100)<15) {
			throw new FalloConexion("FALLO: El "+nombre+", intento conectarse sin �xito mediante una Consola");
		}
		this.getServidor().sala(jugadorconsola);
	}

	public Servidor getServidor() {
		return servidor;
	}

	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}
	

}

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Scanner;

public class Ejercicio8_LecturaArchivo {

	public static void main(String[] args) {
		try(Scanner consola= new Scanner(Paths.get("assets/file1.txt"))){
			System.out.printf("%-10s%-12s%-12s%10s%n","Matricula","Nombre","Apellido","DNI");
			
			while (consola.hasNext()) {
			System.out.printf("%-10d%-12s%-12s%10d%n",
					consola.nextInt(),consola.next(),consola.next(),consola.nextInt());
				
			}		
		} catch (IOException e) {//exception
			
			e.printStackTrace();
		}

	}

}

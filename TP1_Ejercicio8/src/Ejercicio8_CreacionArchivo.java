import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Ejercicio8_CreacionArchivo {

	public static void main(String[] args) {
		try(Formatter output = new Formatter("assets/file1.txt")){
			Scanner consola = new Scanner(System.in);	
			System.out.printf("%s%n%s",
					"Ingrese Matricula, Nombre, Apellido, DNI.",
					"Ctrl+z para Finalizar");
			System.out.println("");
			
            while(consola.hasNext()) {
            	try {
            	output.format("%d %s %s %d %n", 
            			consola.nextInt(), 
            			consola.next(),
            			consola.next(),
            			consola.nextInt()); 
            	//en vez  de consola.next se le pone directo el objeto
            	}
            	catch(NoSuchElementException elementException) {
            		System.err.println("valor invalido, ingrese otra vez");
            		consola.nextLine();
            	}//catch
            	System.out.print("? ");
            }//end while
            consola.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		

	}

}

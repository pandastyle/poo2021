package controlador;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import vista.VistaPrincipal;

public class ControladorVP implements KeyListener {
	private VistaPrincipal vista;
	private String defaul;
	
	public ControladorVP() {
		this.setVista(new VistaPrincipal(this));
		this.getVista().setVisible(true);
	}

	@Override
	public void keyTyped(KeyEvent e) {
		
		/*System.out.println(e.getKeyChar());
		defaul= this.getVista().getTxtBuscador().getText();
		//defaul= defaul+ this.getVista().getLblSearch().getText() ;
		
		this.getVista().getLblSearch().setText(defaul);
		
		//System.out.println("KEYTYPED");*/
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		/*System.out.println(e.getKeyChar());
		defaul= this.getVista().getTxtBuscador().getText();
		
		
		this.getVista().getLblSearch().setText(defaul);*/
		this.getVista().getLblSearch().setText(this.getVista().getTxtBuscador().getText());
		//System.out.println("KEYTYPED");
		
		
	}

	public VistaPrincipal getVista() {
		return vista;
	}

	public void setVista(VistaPrincipal vista) {
		this.vista = vista;
	}

}

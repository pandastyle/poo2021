package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorVP;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Color;

public class VistaPrincipal extends JFrame {

	private JPanel contentPane;
	private ControladorVP controlador;
	private JTextField txtBuscador;
	private JLabel lblSearch;

	
	public VistaPrincipal(ControladorVP controlador) {
		this.setControlador(controlador);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtBuscador = new JTextField();
		
		txtBuscador.addKeyListener(getControlador());
		txtBuscador.setBounds(115, 39, 173, 20);
		contentPane.add(txtBuscador);
		txtBuscador.setColumns(10);
		
		lblSearch = new JLabel("");
		lblSearch.setForeground(Color.BLACK);
		lblSearch.setBackground(Color.BLACK);
		lblSearch.setBounds(115, 87, 173, 20);
		contentPane.add(lblSearch);
		
		this.setLocationRelativeTo(null);
	}


	public ControladorVP getControlador() {
		return controlador;
	}


	public void setControlador(ControladorVP controlador) {
		this.controlador = controlador;
	}


	public JTextField getTxtBuscador() {
		return txtBuscador;
	}


	public void setTxtBuscador(JTextField txtBuscador) {
		this.txtBuscador = txtBuscador;
	}


	public JLabel getLblSearch() {
		return lblSearch;
	}


	public void setLblSearch(JLabel lblSearch) {
		this.lblSearch = lblSearch;
	}
}

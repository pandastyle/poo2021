package Vista;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Controlador.ControladorVP;
import Modelo.Dulce;
import Modelo.InventarioDulces;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JScrollBar;
import java.awt.Color;
import javax.swing.border.BevelBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;

public class VistaPrincipal extends JFrame {

	private JPanel contentPane;
	private JButton btnAgregar;
	private JButton btnModificarInfo;
	private JButton btnEliminarDulce;
	private ControladorVP controlador;
	private DefaultTableModel modeloTabla;
	private JTable table;
	private JButton btnActualizar;
	/**
	 * Launch the application.
	 */
	
	/**
	 * Create the frame.
	 */
	public VistaPrincipal(ControladorVP controlador) {
		this.setControlador(controlador);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 502, 394);
		contentPane = new JPanel();
		contentPane.setBackground(Color.GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblTitulo = new JLabel("INVENTARIO DULCES");
		lblTitulo.setForeground(Color.WHITE);
		lblTitulo.setFont(new Font("Bungee Inline", Font.PLAIN, 17));
		lblTitulo.setBounds(139, 26, 215, 21);
		contentPane.add(lblTitulo);
		
		btnAgregar = new JButton("Agregar Dulce");
		btnAgregar.addActionListener(controlador);
		btnAgregar.setBounds(49, 58, 116, 23);
		contentPane.add(btnAgregar);
		
		btnModificarInfo = new JButton("Modificar Info");
		btnModificarInfo.setBounds(175, 58, 116, 23);
		contentPane.add(btnModificarInfo);
		
		btnEliminarDulce = new JButton("Eliminar Dulce");
		btnEliminarDulce.addActionListener(controlador);
		btnEliminarDulce.setBounds(301, 58, 116, 23);
		contentPane.add(btnEliminarDulce);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(21, 105, 435, 201);
		contentPane.add(scrollPane);
		
		String cabecera[]= {"N�Item","Nombre Producto","Precio","Stock"};
		modeloTabla= new DefaultTableModel(null,cabecera);
		table = new JTable(modeloTabla);
		scrollPane.setViewportView(table);
		
		btnActualizar = new JButton("Actualizar");
		btnActualizar.addActionListener(controlador);
		btnActualizar.setBounds(187, 317, 104, 23);
		contentPane.add(btnActualizar);
		
		
		this.setLocationRelativeTo(null);
	}

	public ControladorVP getControlador() {
		return controlador;
	}

	public void setControlador(ControladorVP controlador) {
		this.controlador = controlador;
	}

	public JButton getBtnAgregar() {
		return btnAgregar;
	}

	public JButton getBtnModificarInfo() {
		return btnModificarInfo;
	}

	public JButton getBtnEliminarDulce() {
		return btnEliminarDulce;
	}

	public DefaultTableModel getModeloTabla() {
		return modeloTabla;
	}

	public void setModeloTabla(DefaultTableModel modeloTabla) {
		this.modeloTabla = modeloTabla;
	}

	public JButton getBtnActualizar() {
		return btnActualizar;
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}
}

package Vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Controlador.ControladorAgregar;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;

public class VistaAgregarDulce extends JFrame {

	private JPanel contentPane;
	private JTextField txtNumero;
	private JTextField txtNombre;
	private JTextField txtPrecio;
	private JTextField txtStock;
	private JButton btnAgregar;
	private ControladorAgregar controlador;

	/**
	 * Launch the application.
	 */
	

	/**
	 * Create the frame.
	 */
	public VistaAgregarDulce(ControladorAgregar controlador) {
		this.setControlador(controlador);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 341, 306);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblagregar = new JLabel("AGREGAR DULCE");
		lblagregar.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblagregar.setBounds(31, 21, 145, 25);
		contentPane.add(lblagregar);
		
		JLabel lblid = new JLabel("Numero Dulce:");
		lblid.setBounds(31, 57, 92, 14);
		contentPane.add(lblid);
		
		txtNumero = new JTextField();
		txtNumero.setBounds(129, 57, 145, 20);
		contentPane.add(txtNumero);
		txtNumero.setColumns(10);
		
		JLabel lblnombre = new JLabel("Nombre Dulce:");
		lblnombre.setBounds(31, 97, 92, 14);
		contentPane.add(lblnombre);
		
		txtNombre = new JTextField();
		txtNombre.setColumns(10);
		txtNombre.setBounds(129, 97, 145, 20);
		contentPane.add(txtNombre);
		
		JLabel lblPrecio = new JLabel("Precio:");
		lblPrecio.setBounds(31, 138, 92, 14);
		contentPane.add(lblPrecio);
		
		txtPrecio = new JTextField();
		txtPrecio.setColumns(10);
		txtPrecio.setBounds(129, 138, 145, 20);
		contentPane.add(txtPrecio);
		
		JLabel lblStock = new JLabel("Stock:");
		lblStock.setBounds(31, 184, 92, 14);
		contentPane.add(lblStock);
		
		txtStock = new JTextField();
		txtStock.setColumns(10);
		txtStock.setBounds(129, 184, 145, 20);
		contentPane.add(txtStock);
		
		btnAgregar = new JButton("Agregar");
		btnAgregar.addActionListener(getControlador());
		btnAgregar.setBounds(109, 220, 89, 23);
		contentPane.add(btnAgregar);
		this.setLocationRelativeTo(null);
	}

	public JButton getBtnAgregar() {
		return btnAgregar;
	}

	public void setBtnAgregar(JButton btnAgregar) {
		this.btnAgregar = btnAgregar;
	}

	public ControladorAgregar getControlador() {
		return controlador;
	}

	public void setControlador(ControladorAgregar controlador) {
		this.controlador = controlador;
	}

	public JTextField getTxtNumero() {
		return txtNumero;
	}

	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public JTextField getTxtPrecio() {
		return txtPrecio;
	}

	public JTextField getTxtStock() {
		return txtStock;
	}
}

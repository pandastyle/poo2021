package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.table.DefaultTableModel;

import Modelo.Dulce;
import Modelo.InventarioDulces;
import Vista.VistaPrincipal;

public class ControladorVP implements ActionListener {
	VistaPrincipal vista;
	InventarioDulces inventario;
	public ControladorVP() {
		
		this.vista = new VistaPrincipal(this);
		this.inventario = new InventarioDulces();
		this.getVista().setVisible(true);
		this.getInventario().setInventario(inventario.bajarArchivo());
		this.actualizarTabla();
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(getVista().getBtnAgregar())) {
			new ControladorAgregar();
			
			inventario.mostrarInfo();
		}else if(e.getSource().equals(getVista().getBtnActualizar())) {
			this.actualizarModelo();
		}else if(e.getSource().equals(getVista().getBtnEliminarDulce())){
			this.getVista().getModeloTabla().removeRow(this.getVista().getTable().getSelectedRow());
		}
		
		
	}
	
	public void actualizarTabla() {
		ArrayList<Dulce> auxiliar= inventario.bajarArchivo();
		for (Dulce dulce : auxiliar) {
			Object[] row= {String.valueOf(dulce.getId_item()),
						   dulce.getNombre_dulce(),"$"+
						   String.valueOf(dulce.getPrecio()),
						   String.valueOf(dulce.getStock())};//se crea una nueva fila.
		this.getVista().getModeloTabla().addRow(row);
		}
	}
	
	public void actualizarModelo() {
		
		String cabecera[]= {"N�Item","Nombre Producto","Precio","Stock"};
		DefaultTableModel modelovacio= new DefaultTableModel(null,cabecera);
		this.getVista().setModeloTabla(modelovacio);
		this.getVista().getTable().setModel(modelovacio);
		this.actualizarTabla();
	}
	public VistaPrincipal getVista() {
		return vista;
	}
	public void setVista(VistaPrincipal vista) {
		this.vista = vista;
	}
	public InventarioDulces getInventario() {
		return inventario;
	}
	public void setInventario(InventarioDulces inventario) {
		this.inventario = inventario;
	}
	
	
	
	
}

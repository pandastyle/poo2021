package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import Modelo.Dulce;
import Modelo.InventarioDulces;
import Vista.VistaAgregarDulce;

public class ControladorAgregar implements ActionListener {
  VistaAgregarDulce vista;
  InventarioDulces inventario;
  public ControladorAgregar() {
	
	this.vista = new VistaAgregarDulce(this);
	this.inventario = new InventarioDulces();
	this.getVista().setVisible(true);
  }
  @Override
	public void actionPerformed(ActionEvent e) {
	if (e.getSource().equals(getVista().getBtnAgregar())) {
		Dulce dul= new Dulce();
		dul.setId_item(Integer.valueOf(getVista().getTxtNumero().getText()));
		dul.setNombre_dulce(getVista().getTxtNombre().getText());
		dul.setPrecio(Double.valueOf(getVista().getTxtPrecio().getText()));
		dul.setStock(Integer.valueOf(getVista().getTxtStock().getText()));
		JOptionPane.showMessageDialog(vista,inventario.agregarDulce(dul));
		this.getVista().setVisible(false);
		this.getVista().dispose();
	}
	
}
public VistaAgregarDulce getVista() {
	return vista;
}
public void setVista(VistaAgregarDulce vista) {
	this.vista = vista;
}
public InventarioDulces getInventario() {
	return inventario;
}
public void setInventario(InventarioDulces inventario) {
	this.inventario = inventario;
}
  
  
}

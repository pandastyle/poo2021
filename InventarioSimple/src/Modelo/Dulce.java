package Modelo;

public class Dulce {
	private Integer id_item;
	private String nombre_dulce;
	private Double precio;
	private Integer stock;
	
	public Dulce() {
		
	}
	public Dulce(Integer id_item, String nombre_dulce, Double precio, Integer stock) {
		
		this.id_item = id_item;
		this.nombre_dulce = nombre_dulce;
		this.precio = precio;
		this.stock = stock;
	}

	public Integer getId_item() {
		return id_item;
	}

	public void setId_item(Integer id_item) {
		this.id_item = id_item;
	}

	public String getNombre_dulce() {
		return nombre_dulce;
	}

	public void setNombre_dulce(String nombre_dulce) {
		this.nombre_dulce = nombre_dulce;
	}

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}
	
	
	
	
	

}

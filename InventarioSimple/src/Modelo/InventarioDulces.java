package Modelo;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Scanner;


public class InventarioDulces {
	private ArrayList<Dulce> inventario;

	public InventarioDulces(ArrayList<Dulce> inventario) {
		
		this.inventario = inventario;
	}

	public InventarioDulces() {
		// TODO Auto-generated constructor stub
	}

	public ArrayList<Dulce> getInventario() {
		return inventario;
	}

	public void setInventario(ArrayList<Dulce> inventario) {
		this.inventario = inventario;
	}
	
	public String agregarDulce(Dulce dulce){
		inventario= bajarArchivo();
		inventario.add(dulce);
       try(Formatter output = new Formatter("assets/dulces.csv")){//crea el archivo nuevamente
			
			for (Dulce dul: inventario) {	
				
				try {
				output.format("%d;%s;%.2f;%d%n", 
						dul.getId_item(), 
						dul.getNombre_dulce(),
						dul.getPrecio(),
						dul.getStock());
						
			    }
				catch(NoSuchElementException elementException) {
				System.err.println("ERROR");

			}//fin exception
				
			}//end for

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return "Dulce Agregado Exitosamente";
		}
		
	
	
	public ArrayList<Dulce> bajarArchivo(){
		ArrayList<Dulce> arrayAux= new ArrayList<Dulce>();
		
		try(Scanner consola= new Scanner(Paths.get("assets/dulces.csv"))){
			consola.useDelimiter("\\r\\n|\\n\\r");//hace separacion de datos por lineas
			String[] datos;
 			while (consola.hasNext()) {
 				datos = consola.next().split(";"); 
 				
 				Dulce dulAux= new Dulce();
 				dulAux.setId_item(Integer.valueOf(datos[0]));
 				dulAux.setNombre_dulce(datos[1]);
 				dulAux.setPrecio(Double.valueOf(datos[2].replace(",",".")));
 				dulAux.setStock(Integer.valueOf(datos[3]));
 				arrayAux.add(dulAux);
 
			}		
		} catch (IOException e) {//exception
			
			e.printStackTrace();
		}
		return arrayAux;
	}
	
	public void mostrarInfo() {
		for (Dulce dulce : inventario) {
			System.out.println("nombre: "+ dulce.getNombre_dulce()+ " precio: $"+dulce.getPrecio()+ " stock:"+ dulce.getStock());
		}
	}
}

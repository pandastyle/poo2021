import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class Ventana extends JFrame {

	private JPanel contentPane;
	private JTextField txtNombre;
	private JTextField txtApellido;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana frame = new Ventana();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ventana() {
		setTitle("Saludo");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 392, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(114, 110, 46, 14);
		contentPane.add(lblNombre);
		
		txtNombre = new JTextField();
		lblNombre.setLabelFor(txtNombre);
		txtNombre.setBounds(176, 107, 86, 20);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);
		
		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setBounds(114, 135, 46, 14);
		contentPane.add(lblApellido);
		
		txtApellido = new JTextField();
		lblApellido.setLabelFor(txtApellido);
		txtApellido.setBounds(176, 132, 86, 20);
		contentPane.add(txtApellido);
		txtApellido.setColumns(10);
		
		JButton btnSaludo = new JButton("Saludar");
		btnSaludo.addActionListener(new ActionListener() {
			
			
			public void actionPerformed(ActionEvent e) {
				saludar();
				
			}
		});
		
		btnSaludo.setBounds(131, 196, 89, 23);
		contentPane.add(btnSaludo);
		
		JLabel lblImagen = new JLabel("");
		ImageIcon imageIcon= new ImageIcon(new ImageIcon("img\\mario.jfif").getImage().getScaledInstance(106, 73, Image.SCALE_DEFAULT));
		lblImagen.setIcon(imageIcon);
		lblImagen.setBounds(129, 23, 106, 73);
		contentPane.add(lblImagen);
		
		setLocationRelativeTo(null);//muestra en el medio
	}

	protected void saludar() {
	
	 
	 JOptionPane.showMessageDialog(this, "Bienvenido "+txtNombre.getText()+" "+txtApellido.getText());
	 
		
	}
	
}

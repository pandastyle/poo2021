import java.util.LinkedList;

public class Almacen {
private LinkedList<Contenedor> deposito= new LinkedList<Contenedor>();

public synchronized Contenedor obtenerContenedor() {
	
	while (this.getDeposito().isEmpty()) {
		try {
			wait();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	Contenedor contenedor= deposito.removeFirst();
	
	if (contenedor!=null) {
		System.out.println("<--RETIRA: "+contenedor.toString());
	}
	notifyAll();
	return contenedor;
}

public	synchronized void almacenarContenedor(Contenedor contenedor) {
	
	while (this.getDeposito().size()>=3) {
		try {
			wait();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	if (contenedor!=null) {
		System.out.println("-->ALMACENO ("+contenedor.getRecepcionista()+"): "+contenedor.toString());
	}
	
	this.getDeposito().add(contenedor);
	notifyAll();
}

public LinkedList<Contenedor> getDeposito() {
	return deposito;
}

public void setDeposito(LinkedList<Contenedor> deposito) {
	this.deposito = deposito;
}


}


import java.util.Random;

public class Recepcionista extends Thread {
	private String nombre;
	private Almacen almacen;
	
	
	public Recepcionista(String nombre, Almacen almacen) {
		super();
		this.nombre = nombre;
		this.almacen = almacen;
	}
	
	public Contenedor recepcionar() {
		
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Integer peso= new Random().nextInt(51)+10;
		Integer valorDestino= new Random().nextInt(4);
		String destino="";
		switch (valorDestino) {
		case 0: {
			destino="Comodoro Rivadavia";
			break;
		}
		case 1: {
			destino="Mar del Plata";	
			break;
				}
		case 2: {
			destino="Puerto Madryn";
			break;
		}
		case 3: {
			destino="Bahia Blanca";
			break;
		}
		default:
			throw new IllegalArgumentException("Unexpected value: " + valorDestino);
		}
		Contenedor contenedor= new Contenedor(peso,destino,this.getNombre());
		
		return contenedor;
	}
	
	public void enviarRecepcion(Contenedor contenedor) throws FallaTrasladoException {
		Integer num=new Random().nextInt(100);
	
		if (num<35) {
			 throw new FallaTrasladoException();
		}
		this.getAlmacen().almacenarContenedor(contenedor);
	}
	
	public void run() {
		for (int i = 0; i < 5; i++) {
			try {
				this.enviarRecepcion(this.recepcionar());
			} catch (FallaTrasladoException e) {
				System.out.println("ERROR: no se encontro el contenedor");
			}
		}
		this.getAlmacen().almacenarContenedor(null);
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Almacen getAlmacen() {
		return almacen;
	}
	public void setAlmacen(Almacen almacen) {
		this.almacen = almacen;
	}
	
	
}

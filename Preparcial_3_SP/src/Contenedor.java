
public class Contenedor {
	private Integer peso;
	private String destino;
	private String recepcionista;
	
	public Contenedor(Integer peso, String destino, String recepcionista) {
		super();
		this.peso = peso;
		this.destino = destino;
		this.recepcionista = recepcionista;
	}
	@Override
	public String toString() {
		return "Contenedor [peso=" + peso + ", destino=" + destino + "]";
	}
	public Integer getPeso() {
		return peso;
	}
	public void setPeso(Integer peso) {
		this.peso = peso;
	}
	public String getDestino() {
		return destino;
	}
	public void setDestino(String destino) {
		this.destino = destino;
	}
	public String getRecepcionista() {
		return recepcionista;
	}
	public void setRecepcionista(String recepcionista) {
		this.recepcionista = recepcionista;
	}
	
	
	
}

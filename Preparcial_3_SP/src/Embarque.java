import java.util.ArrayList;

public class Embarque extends Thread implements Trasladar{
	private ArrayList<Contenedor> colCon= new ArrayList<Contenedor>();
	private Almacen almacen;
	
	public Embarque(Almacen almacen) {
		super();
		this.almacen = almacen;
	}
	
	public void informe() {
		Integer apto=0;
		
		for (Contenedor contenedor : colCon) {
			if (contenedor.getPeso()<40) {
				apto++;
			}
		}
		System.out.println("INFORME:\n"+"de los "+this.getColCon().size()+", hay "+apto+" habilitados para ser enviados.");	
	}
	public void enviarContenedor(Contenedor contenedor) {
		
	}
	
	public void run() {
		Integer contador=0;
		
		while (contador<2) {
			Contenedor contenedor= this.getAlmacen().obtenerContenedor();
			if (contenedor!=null) {
				this.enviarContenedor(contenedor);
				this.getColCon().add(contenedor);
			}else {
				contador++;
			}
		}
		this.informe();
	}

	public ArrayList<Contenedor> getColCon() {
		return colCon;
	}

	public void setColCon(ArrayList<Contenedor> colCon) {
		this.colCon = colCon;
	}

	public Almacen getAlmacen() {
		return almacen;
	}

	public void setAlmacen(Almacen almacen) {
		this.almacen = almacen;
	}
	
	
	
	
	
	
}

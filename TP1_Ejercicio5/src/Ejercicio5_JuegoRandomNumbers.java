import java.security.SecureRandom;
import java.util.Scanner;

public class Ejercicio5_JuegoRandomNumbers {

	public static void main(String[] args) {
		Scanner consola = new Scanner(System.in);
		Boolean rta= true;
		Integer numAle1= 0;
		Integer numAle2= 0;
		Integer numelegido;
		Integer cntVictorias=0;
		Integer cntPerdidas=0;
		
		SecureRandom rnd = new SecureRandom();
		
		System.out.println("--------------------------------");
		System.out.println("Bienvenido Al Mayor o Menor Incognito");
		while (rta) {
			
		while (numAle1==numAle2) {
			numAle1= rnd.nextInt(10)+1;
			numAle2= rnd.nextInt(10)+1;
		}
		
		
		System.out.println("Debes Elegir uno de los 2 valores ocultos, El Mayor Gana!");
		System.out.println("Valor 1= ?            Valor 2=?");
		
		numelegido= consola.nextInt();
		
		switch (numelegido) {
		case 1: {
			if (numAle1>numAle2) {
				System.out.println("Ganaste Valor 1="+numAle1+"    Valor 2="+numAle2);
				cntVictorias++;
			}else {
				System.err.println("Perdiste Valor 1="+numAle1+"   Valor 2="+numAle2);
				cntPerdidas++;
			}
			break;
		}
		case 2: {
			if (numAle2>numAle1) {
				System.out.println("Ganaste Valor 1="+numAle1+"     Valor 2="+numAle2);
				cntVictorias++;
			}else {
				System.err.println("Perdiste Valor 1="+numAle1+"    Valor 2="+numAle2);
				cntPerdidas++;
			}
			break;
		}
		default:
			throw new IllegalArgumentException("Unexpected value: " + numelegido);
		}

		System.out.println("Jugar denuevo?si/no");
		if (consola.next().equalsIgnoreCase("no")) {
			rta=false;
		}
		System.out.println("--------------------------------");
		}
		
		System.out.println("|Game Over| Partidas Ganadas= "+cntVictorias+" Partidas Perdidas= "+cntPerdidas);
		consola.close();

	}

}

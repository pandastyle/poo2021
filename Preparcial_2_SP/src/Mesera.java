import java.util.LinkedList;

public class Mesera {
	private LinkedList<Pedido> listaPedidos= new LinkedList<Pedido>();
	
	public synchronized void tomarOrden(Pedido pedido) {
		while(listaPedidos.size()>=2) {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (pedido != null) {
			System.out.println("TOMA PEDIDO: Pedido de:"+ pedido.toString());
		}
		this.getListaPedidos().add(pedido);
		notifyAll();
	}
	
	public synchronized Pedido entregarACocina() {
		while (this.getListaPedidos().isEmpty()) {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		}
		notifyAll();
		return this.getListaPedidos().removeFirst();
	}

	public LinkedList<Pedido> getListaPedidos() {
		return listaPedidos;
	}

	public void setListaPedidos(LinkedList<Pedido> listaPedidos) {
		this.listaPedidos = listaPedidos;
	}
	
	
}

import java.util.Random;

public class Delivery extends Thread implements Atendible {
	private Mesera mesera;

	public Delivery(Mesera mesera) {
		
		this.mesera = mesera;
	}

	
	@Override
	public Pedido generarPedido() {
		// TODO Auto-generated method stub
		return new Pedido("Delivery","un pedido de comida");
	}


	public void run() {
		Random ran = new Random();
		Integer num= ran.nextInt(5);
		for (int i = 0; i < num; i++) {
			this.getMesera().tomarOrden(this.generarPedido());
		}
		this.getMesera().tomarOrden(null);
	}
	public Mesera getMesera() {
		return mesera;
	}

	public void setMesera(Mesera mesera) {
		this.mesera = mesera;
	}
}

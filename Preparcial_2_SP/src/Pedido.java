
public class Pedido {
	
	 private String origen;
	 private String pedido;
	 
	 
	public Pedido(String origen, String pedido) {
		super();
		this.origen = origen;
		this.pedido = pedido;
	}
	public String getOrigen() {
		return origen;
	}
	public void setOrigen(String origen) {
		this.origen = origen;
	}
	public String getPedido() {
		return pedido;
	}
	public void setPedido(String pedido) {
		this.pedido = pedido;
	}
	@Override
	public String toString() {
		return  origen + ", Pedido:" + pedido  ;
	}
	 
	 
}

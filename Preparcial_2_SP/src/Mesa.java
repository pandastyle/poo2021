import java.util.Random;

public class Mesa implements Atendible,Runnable {
	private Integer nro;
	private Mesera mesera;

	public Mesa(Integer nro, Mesera mesera) {
		super();
		this.nro = nro;
		this.mesera = mesera;
	}

	@Override
	public void run() {
		Random ran = new Random();
		Integer num= ran.nextInt(8);
		for (int i = 0; i < num; i++) {
			this.getMesera().tomarOrden(this.generarPedido());
		}
		this.getMesera().tomarOrden(null);
	}

	@Override
	public Pedido generarPedido() {
		// TODO Auto-generated method stub
		return new Pedido("Mesa nro:"+this.getNro(),"un pedido de comida");
	}

	public Integer getNro() {
		return nro;
	}

	public void setNro(Integer nro) {
		this.nro = nro;
	}

	public Mesera getMesera() {
		return mesera;
	}

	public void setMesera(Mesera mesera) {
		this.mesera = mesera;
	}
	

}

import java.util.Random;

public class Cocina extends Thread {
	private Mesera mesera;

	public Cocina(Mesera mesera) {
		super();
		this.mesera = mesera;
	}

	public void prepararPedido(Pedido pedido) throws PerdidaDePedido {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Random ran = new Random();
		
		if (ran.nextInt(100)<20) {
			throw new PerdidaDePedido("PERDIDA: La cocina perdio el pedido de: "+pedido.toString());
		}
		System.out.println("PREPARANDO en cocina Pedido de: "+ pedido.toString());
		
	}
	
	public void run() {
		Integer contador=0;
		while (contador<2) {
			Pedido pedido= this.getMesera().entregarACocina();
			if (pedido!=null) {
				try {
					this.prepararPedido(pedido);
				} catch (PerdidaDePedido e) {
					System.out.println(e);
				}
			}else{
				contador++;
			}
			
		}
		System.out.println("Cocina Cerrada");
		
	}
	public Mesera getMesera() {
		return mesera;
	}

	public void setMesera(Mesera mesera) {
		this.mesera = mesera;
	}
	
	
}

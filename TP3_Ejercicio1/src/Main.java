import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		Leon leo= new Leon("Leon");
		Pajaro p= new Pajaro("Aguila");
		Gato cat= new Gato("Gato");
		
		ArrayList<Animal> animales= new ArrayList<Animal>();
		animales.add(leo);
		animales.add(cat);
		animales.add(p);
		
		for (Animal animal : animales) {
			System.out.println(animal.emitirSonido());
			System.out.println(animal.obtenerAlimento());
		}

	}

}

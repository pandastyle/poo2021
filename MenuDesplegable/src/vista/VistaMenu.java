package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Controlador.ControladorMenu;
import java.awt.Color;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.SwingConstants;
import javax.swing.JMenuBar;
import javax.swing.JButton;

public class VistaMenu extends JFrame {

	private JPanel contentPane;
	private ControladorMenu controlador;
	private JPanel panelMenu;
	private JLabel lblMenu;
	private JMenuBar menuBar;
	private JMenu mnNewMenu;
	private JMenuItem mntmNewMenuItem;
	private JButton btnperfil;
	private JButton btncuenta;
	private JButton btnsesion;
	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VistaMenu frame = new VistaMenu();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
*/
	/**
	 * Create the frame.
	 */
	public VistaMenu(ControladorMenu controlador) {
		this.setControlador(controlador);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 568, 360);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		panelMenu = new JPanel();
		panelMenu.setBackground(Color.BLACK);
		panelMenu.setBounds(257, 0, 149, 110);
		contentPane.add(panelMenu);
		panelMenu.setLayout(null);
		
		lblMenu = new JLabel("");
		lblMenu.setBounds(0, 0, 149, 37);
		
		panelMenu.add(lblMenu);
		//lblMenu.setIcon(menu);
		
		menuBar = new JMenuBar();
		menuBar.setBounds(40, 82, 147, 37);
		
		contentPane.add(menuBar);
		
		mnNewMenu = new JMenu("                                         ");
		menuBar.add(mnNewMenu);
		
		mntmNewMenuItem = new JMenuItem("NuevoItem");
		mnNewMenu.add(mntmNewMenuItem);
		ImageIcon menu= new ImageIcon(new ImageIcon("img/micuenta.png").getImage());
		btncuenta = new JButton("");
		btncuenta.setBounds(257, 146, 147, 37);
		btncuenta.setIcon(menu);
		btncuenta.addActionListener(controlador);
		contentPane.add(btncuenta);
		
		btnperfil = new JButton("PERFIL");
		btnperfil.setBackground(Color.WHITE);
		btnperfil.addActionListener(controlador);
		btnperfil.setBounds(257, 183, 147, 37);
		contentPane.add(btnperfil);
		
		btnsesion = new JButton("Cerrar Sesion");
		btnsesion.setBackground(Color.WHITE);
		btnsesion.setBounds(257, 218, 147, 37);
		contentPane.add(btnsesion);
		btnperfil.setVisible(false);
		btnsesion.setVisible(false);
		
		this.setLocationRelativeTo(null);
	}
	public ControladorMenu getControlador() {
		return controlador;
	}
	public void setControlador(ControladorMenu controlador) {
		this.controlador = controlador;
	}
	public JPanel getPanelMenu() {
		return panelMenu;
	}
	public void setPanelMenu(JPanel panelMenu) {
		this.panelMenu = panelMenu;
	}
	public JLabel getLblMenu() {
		return lblMenu;
	}
	public void setLblMenu(JLabel lblMenu) {
		this.lblMenu = lblMenu;
	}
	public JButton getBtnperfil() {
		return btnperfil;
	}
	public void setBtnperfil(JButton btnperfil) {
		this.btnperfil = btnperfil;
	}
	public JButton getBtncuenta() {
		return btncuenta;
	}
	public void setBtncuenta(JButton btncuenta) {
		this.btncuenta = btncuenta;
	}
	public JButton getBtnsesion() {
		return btnsesion;
	}
	public void setBtnsesion(JButton btnsesion) {
		this.btnsesion = btnsesion;
	}
}

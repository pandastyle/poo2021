package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JOptionPane;

import desplazable.Desface;
import vista.VistaMenu;



public class ControladorMenu implements ActionListener {
	//private Desface desplace;
	private VistaMenu vista;
	
	

	public ControladorMenu() {
		
		this.vista = new VistaMenu(this);
		this.vista.setVisible(true);
		//desplace= new Desface();
	}

	

	public VistaMenu getVista() {
		return vista;
	}

	public void setVista(VistaMenu vista) {
		this.vista = vista;
	}



	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(getVista().getBtncuenta())) {
			if (this.getVista().getBtnperfil().isVisible()&&this.getVista().getBtnsesion().isVisible()) {
				this.getVista().getBtnperfil().setVisible(false);
				this.getVista().getBtnsesion().setVisible(false);
			}else {
				this.getVista().getBtnperfil().setVisible(true);
				this.getVista().getBtnsesion().setVisible(true);
			}
			
		}
		
		if (e.getSource().equals(getVista().getBtnperfil())) {
			JOptionPane.showMessageDialog(vista, "SALTO UN MENSAJE");
		}
		
	}
	

}

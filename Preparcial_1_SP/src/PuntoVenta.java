

import java.util.concurrent.ThreadLocalRandom;

public class PuntoVenta extends Thread{
private MaquinaFiscal maquina;

public PuntoVenta(MaquinaFiscal maquina) {
	super();
	this.maquina = maquina;
}
public MaquinaFiscal getMaquina() {
	return maquina;
}
public void run() {
	for (int i = 0; i < 10; i++) {
			this.realizarVenta();
		}
	this.getMaquina().vender(null);
}

public void realizarVenta() {
	int numero = ThreadLocalRandom.current().nextInt(99999);
	Venta venta = new Venta(numero);
	this.getMaquina().vender(venta); 
}
public void setMaquina(MaquinaFiscal maquina) {
	this.maquina = maquina;
}


}



import java.util.concurrent.ThreadLocalRandom;


public class Cine extends Thread {
private Integer asientos=0;
private MaquinaFiscal maquina;


public Cine(MaquinaFiscal maquina) {
	this.maquina = maquina;
	
}
public void run() {
	int contador = 0;
	while(contador < 2) {
		Venta venta = this.maquina.consultar();
		if (venta != null) {
			this.setAsientos(this.getAsientos()+1);
		}else {
			contador ++;
		}
	}
	if (this.getAsientos() == 20) {
		System.out.println("Sala llena");
	}
	this.informe();
}
public void informe() {
	int cantAsientosNoOcupados = 0;
	while(this.getAsientos() > 0) {
		int random =ThreadLocalRandom.current().nextInt(100);
		if(random< 20) {
			cantAsientosNoOcupados++;
		}
		this.setAsientos(this.getAsientos()-1);
	}

	System.out.println("Informe de asientos vendidos, pero no ocupados:\r\n"
			+ "La cantidad de los asientos que no fueron ocupados, pero si vendidos durante\r\n"
			+ "la funci�n fue de "+cantAsientosNoOcupados);
}

public Integer getAsientos() {
	return asientos;
}
public void setAsientos(Integer asientos) {
	this.asientos = asientos;
}
public MaquinaFiscal getMaquina() {
	return maquina;
}
public void setMaquina(MaquinaFiscal maquina) {
	this.maquina = maquina;
}

}



public class Venta {
private Integer numero;
private String fecha = "24/10/2017 19:50";

public Venta(Integer numero) {
	this.numero = numero;
}
public Integer getNumero() {
	return numero;
}
public void setNumero(Integer numero) {
	this.numero = numero;
}
public String getFecha() {
	return fecha;
}
public void setFecha(String fecha) {
	this.fecha = fecha;
}
@Override
public String toString() {
	return "Ticket:\nPelicula: El se�or de los anillos\n"+  "numero= " + numero + ", fecha=" + fecha + "]";
}

}

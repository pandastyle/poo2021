

public class Main {

	public static void main(String[] args) {
		MaquinaFiscal maquina = new MaquinaFiscal();
		new PuntoVenta(maquina).start();
		new Online(maquina).start();
		new Cine(maquina).start();
	}

}

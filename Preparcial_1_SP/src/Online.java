

import java.util.concurrent.ThreadLocalRandom;

public class Online extends PuntoVenta {

	public Online(MaquinaFiscal maquina) {
		super(maquina);
	}
	public void realizarVenta() {
		super.realizarVenta();
		int numero_2 = ThreadLocalRandom.current().nextInt(900)+99;
		System.out.println("CODIGO PARA RETIRAR ENTRADA = ELS"+numero_2+"\n");	
	}
	
	public void run() {
		for (int i = 0; i < 10; i++) {
			try {
				this.revisarConexion();
				this.realizarVenta();
			} catch (FallaConexion e) {
				System.out.println("FALLO: Fall� la conexi�n con el servidor.");
			}
		}  
		this.getMaquina().vender(null);
	}
	public void revisarConexion() throws FallaConexion {
		if (ThreadLocalRandom.current().nextInt(100)<15) {
			throw new FallaConexion();
		}
	}
	
}

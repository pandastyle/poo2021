
public class Paquete extends Correspondencia {
	private Double peso;

	public Paquete(String remitente, String destinatario, String direccion, String codigoPostal, Double peso) {
		super(remitente, destinatario, direccion, codigoPostal);
	this.setPeso(peso);	
	}

	public Double getPeso() {
		return peso;
	}

	public void setPeso(Double peso) {
		this.peso = peso;
	}

	@Override
	public String toString() {
		return "Paquete --> "+ super.toString()+" peso=" + peso ;
	}

	}

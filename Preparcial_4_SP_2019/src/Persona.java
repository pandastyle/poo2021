import java.util.Iterator;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Persona implements Runnable{
	private String nombre;
	private Buzon buzon;
	
	
	public Persona(String nombre, Buzon buzon) {
		super();
		this.nombre = nombre;
		this.buzon = buzon;
	}
	@Override
	public void run() {
		
	for (int i = 0; i < 5; i++) {
		this.getBuzon().depositar(this.crearCorrespondencia());
	}
	this.getBuzon().depositar(null);
		
	}
	
	private Correspondencia crearCorrespondencia() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		Integer valorDestino= new Random().nextInt(4);
		String destinatario,direccion,texto;
		switch (valorDestino) {
		case 0: {
			destinatario="destinatario 1";
			direccion="direccion 1";
			texto="texto 1";
			break;
		}
		case 1: {
			destinatario="destinatario 2";	
			direccion="direccion 2";
			texto="texto 2";
			break;
				}
		case 2: {
			destinatario="destinatario 3";
			direccion="direccion 3";
			texto="texto 3";
			break;
		}
		case 3: {
			destinatario="destinatario 4";
			direccion="direccion 4";
			texto="texto 4";
			break;
		}
		default:
			throw new IllegalArgumentException("Unexpected value: " + valorDestino);
		}
		
		Double peso= (ThreadLocalRandom.current().nextDouble(4.0))+1;
		String codPostal;
		if (new Random().nextInt(100)<20) {
			codPostal="9001";
		}else {
			codPostal="9000";
		}
		
		Correspondencia correspondencia;
		if (new Random().nextInt(100)<50) {
			correspondencia= new Carta(this.getNombre(),destinatario,direccion,codPostal,texto);
		}else {
			correspondencia= new Paquete(this.getNombre(),destinatario,direccion,codPostal,peso);
		}
		return correspondencia;
			
		
		
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Buzon getBuzon() {
		return buzon;
	}
	public void setBuzon(Buzon buzon) {
		this.buzon = buzon;
	}
	
	
}

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class Cartero extends Thread {
 private HashMap<String, LinkedList<Correspondencia>> aEnviar = new HashMap<String, LinkedList<Correspondencia>>();
 private Integer cpInvalidos;
 private Buzon buzon;
 
	public Cartero(Buzon buzon) {
	super();
	this.buzon = buzon;
	this.setCpInvalidos(0);
	}
	
	public String informeEnvios() {
		String mensaje="INFORME DE ENVIOS: \n";
		for (Map.Entry<String,LinkedList<Correspondencia> > entrada : this.aEnviar.entrySet()) {//entrada dentro del hashmap
			//obtiene el objeto de la key entrada.getKey()
			//obtiene el objeto de valor entrada.getValue();
			
			mensaje= mensaje + entrada.getValue().size() +" elementos de correspondencia enviados a " + entrada.getKey()+ "\n";
		}
		mensaje= mensaje + this.getCpInvalidos()+" envios con codigo postal invalido";
		return mensaje;
	}
	
	public void run() {
		Integer contador=0;
		while (contador<3) {
			try {
				Correspondencia c= this.getBuzon().retirar();
				if (c!=null) {
					if (aEnviar.containsKey(c.getDestinatario())) {//si existe el destinatario le agrega su correspondencia
						aEnviar.get(c.getDestinatario()).add(c);//agarra la lista de la key asociada y le agrega un elemento
					}else {//si no existe el destinatario, crea el espacio en el hash y le agrega su correspondencia
						LinkedList<Correspondencia> paqueteria= new LinkedList<Correspondencia>();
						paqueteria.add(c);
						aEnviar.put(c.getDestinatario(), paqueteria);
					}
				}else {
					contador++;
				}
			} catch (CodigoPostalInvalidoException e) {
				System.out.println("No se pudo enviar la correspondencia, c�digo postal invalido");
				this.setCpInvalidos(getCpInvalidos()+1);
			}	
		}
		
		System.out.println(this.informeEnvios());
	}
	
	public HashMap<String, LinkedList<Correspondencia>> getaEnviar() {
		return aEnviar;
	}
	public void setaEnviar(HashMap<String, LinkedList<Correspondencia>> aEnviar) {
		this.aEnviar = aEnviar;
	}
	public Integer getCpInvalidos() {
		return cpInvalidos;
	}
	public void setCpInvalidos(Integer cpInvalidos) {
		this.cpInvalidos = cpInvalidos;
	}
	public Buzon getBuzon() {
		return buzon;
	}
	public void setBuzon(Buzon buzon) {
		this.buzon = buzon;
	}
 
 
}

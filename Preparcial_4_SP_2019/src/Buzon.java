import java.util.LinkedList;

public class Buzon {
	private LinkedList<Correspondencia> correspondencia= new LinkedList<Correspondencia>();
	
	 public synchronized void depositar(Correspondencia c) {
		 while (this.getCorrespondencia().size()>=5) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}	
		}
		 if (c!=null) {
			System.out.println("PERSONA DEPOSITA "+ c.toString());
		}
		 this.getCorrespondencia().add(c);
		 notifyAll();
	 }
	 
	 public synchronized Correspondencia retirar() throws CodigoPostalInvalidoException  {
		 while (this.getCorrespondencia().isEmpty()) {
			 try {
					wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
		}
		 
		 Correspondencia c= this.getCorrespondencia().removeFirst();
		 
		 
		 if (c!=null) {
				System.out.println("CARTERO RETIRA "+ c.toString());
				if (c.getCodigoPostal().equals("9001")) {
					throw new CodigoPostalInvalidoException();
			    }
				
		 }
		 notifyAll();
		 return c;
	 }

	public LinkedList<Correspondencia> getCorrespondencia() {
		return correspondencia;
	}

	public void setCorrespondencia(LinkedList<Correspondencia> correspondencia) {
		this.correspondencia = correspondencia;
	}
	 
	 
	 
}

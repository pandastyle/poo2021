import java.util.ArrayList;

public class Ejercicio7_BreakPoint {
public static void main(String[] args) {
	
	ArrayList<String> cadena= new ArrayList<>();
	String cad= "cad";
	
	for (int i = 0; i < 5; i++) {
		cadena.add(cad+i);
	}
	
	for (String string : cadena) {
		System.out.println(string);
	}
}
}

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Scanner;

public class EjemploLecturaCSV {

	public static void main(String[] args) {
		try(Scanner consola= new Scanner(Paths.get("assets/probando.csv"))){
			System.out.printf("%-10s%-12s%-12s%10s%n","cuenta","nombre","apellido","balance");
			
			consola.useDelimiter("\\r\\n|\\n\\r");//hace separacion de datos por lineas
			String[] datos;
 			while (consola.hasNext()) {
 				datos = consola.next().split(";"); 
 				//el .split sirve para cortar la cadena donde haya un ;
			System.out.println(datos[0] + "         " + 
 				datos[1] + "     " + datos[2]+ "     "+ 
			    Double.valueOf(datos[3].replace(",", ".")));
			//Integer.valueOf(datos[2]) sirve para pasar de String a Integer
			}		
		} catch (IOException e) {//exception
			
			e.printStackTrace();
		}

	}

}

import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class EjemploCreacionCSV {
public static void main(String[] args) {
	try(Formatter output = new Formatter("assets/probando.csv")){
		Scanner consola = new Scanner(System.in);
		System.out.printf("%s%n%sn?",
				"Ingrese numero de cuenta, nombre, apellido, balance.",
				"ingrese ctrl+z para finalizar.");
		
		while (consola.hasNext()) {//mientras existan elementos
			try {
			output.format("%d;%s;%s;%.2f%n", consola.nextInt(), consola.next(),consola.next(), consola.nextDouble());	
		    }
			catch(NoSuchElementException elementException) {
			System.err.println("Valor Invalido. Ingrese otra vez");
			consola.nextLine();
		}//fin exception
			System.out.println("?");
			}
		
	} catch (FileNotFoundException e) {
		e.printStackTrace();
	}
	
}
}

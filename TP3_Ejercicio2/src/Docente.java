
public class Docente extends Personal{
	
	private Boolean espacio;//true 40hs false el 60%
	
	@Override
	public void calcuHorario(CargaHoraria carga) {
		
	switch (carga) {
	case SIMPLE: {
		super.setHoras(10);
		break;
		
	}
    case SEMIEXCLUSIVA: {
    	super.setHoras(20);
		break;
		
	}
    case EXCLUSIVA: {
    	super.setHoras(40);
		break;
		
	}
	default:
		throw new IllegalArgumentException("Unexpected value: " + carga);
	}
		
	}
	
	public void espacioFisico(Boolean confirmacion) {
		Integer porcentaje=(int) (40*0.6);
		espacio=confirmacion;//posteriormente se sabe  si tiene espacio
		
		if (confirmacion) {
			this.calcuHorario(CargaHoraria.EXCLUSIVA);;//se le pone la carga exclusiva
		}else {
			
			super.setHoras(porcentaje);
		}
	}

}

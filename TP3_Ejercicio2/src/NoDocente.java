
public class NoDocente extends Personal {
	
	@Override
	public void calcuHorario(CargaHoraria carga) {
	switch (carga) {
	case JLABORALND: {
		super.setHoras(30);
		break;
	}
	case JREDUCIDA: {
		super.setHoras(20);
		break;
	}
	default:
		throw new IllegalArgumentException("Unexpected value: " + carga);
	}
		
	}
}


public abstract class Personal {
	private String nombre;
	private String sector;
	private String antiguedad;
	//private CargaHoraria horas_trabajadas;
	private Integer horas;

	
	public abstract void calcuHorario(CargaHoraria carga);


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getSector() {
		return sector;
	}


	public void setSector(String sector) {
		this.sector = sector;
	}


	public String getAntiguedad() {
		return antiguedad;
	}


	public void setAntiguedad(String antiguedad) {
		this.antiguedad = antiguedad;
	}


	public Integer getHoras() {
		return horas;
	}


	public void setHoras(Integer horas) {
		this.horas = horas;
	}
	
	
}

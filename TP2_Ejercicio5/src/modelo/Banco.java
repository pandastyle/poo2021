package modelo;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Banco {
	private String nombre;
	HashMap<String, Persona> cuentas;
	
	public Banco() {
		this("", new HashMap<String,Persona>());
	}
	public Banco(String nombre, HashMap<String,Persona> cuentas) {
		
		this.nombre = nombre;
		this.cuentas = cuentas;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public HashMap<String, Persona> getCuentas() {
		return cuentas;
	}
	public void setCuentas(HashMap<String, Persona> cuentas) {
		this.cuentas = cuentas;
	}
	
	/*public Boolean agregarCuenta(Persona persona) {///ahora aplicalo haciendo en csv
		if (cuentas.containsKey(persona.getNombre())) {
			return false;
		}else {
		cuentas.put(persona.getNombre(), persona);
		return true;
		}
	}*/
	
	public Boolean agregarCuenta(Persona persona) {///ahora aplicalo haciendo en csv
		cuentas= this.bajarArchivo();
		Persona perAux;
		
		if (cuentas.containsKey(persona.getNombre())) {
			return false;//si ya existe retorna false
		}else {
		cuentas.put(persona.getNombre(), persona);
		try(Formatter output = new Formatter("assets/clients.csv")){//crea el archivo nuevamente
			
			for (Map.Entry<String,Persona> entrada : this.cuentas.entrySet()) {	
				perAux= entrada.getValue();// se le da la persona dentro del hash
				try {
				output.format("%s;%s;%s;%s;%.2f %n", //carga los datos al archivo
						perAux.getNombre(), 
						perAux.getGenero(),
						perAux.getEmail(),
						perAux.getPassword(),
						perAux.getSaldo());	
			    }
				catch(NoSuchElementException elementException) {
				System.err.println("ERROR");

			}//fin exception
				
			}//end for

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return true;
		}
		
		
		
	}
	
	
	
	public Boolean eliminarCuenta(Persona persona) {
		this.getCuentas().remove(persona.getNombre());
		return true;
	}
	
	public void listarClientes() {
		Persona perCuenta= new Persona();
		String nombre;
		for (Map.Entry<String,Persona> entrada : this.cuentas.entrySet()) {//entrada dentro del hashmap
			nombre=entrada.getKey();//obtiene el objeto de la key
			perCuenta= entrada.getValue();//obtiene el objeto de valor
			//System.out.println("id= " + id +" Nombre Cliente: "+ perCuenta.getNombre()+" Saldo: "+perCuenta.getSaldo());
			System.out.println(perCuenta.toString());
		}
	}
	
	
	
	/*public Double getSaldo(Persona persona) {
		Persona perCuenta= new Persona();
		
		if (cuentas.containsKey(persona.getNombre())) {//verifica si existe dentro del hashmap

		for (Map.Entry<String, Double> entrada : this.cuentas.entrySet()) {//entrada dentro del hashmap
			perCuenta.setNombre(entrada.getKey());
			if (perCuenta.getNombre().equalsIgnoreCase(persona.getNombre())) {
				return entrada.getValue();//obtiene el objeto de valor
			}
		}
		}
		return -1.0;
	}*/
	
	//mihash.get(ValorKey); obtiene el valor almacenado en el hash de la posicion del key
	
	public HashMap<String,Persona> bajarArchivo(){
		HashMap<String,Persona> hashAux= new HashMap<String,Persona>();
		
		try(Scanner consola= new Scanner(Paths.get("assets/clients.csv"))){
			consola.useDelimiter("\\r\\n|\\n\\r");//hace separacion de datos por lineas
			String[] datos;
 			while (consola.hasNext()) {
 				datos = consola.next().split(";"); 
 				//carga los datos en el cliente auxiliar
 				Persona clienteAux= new Persona();
 				clienteAux.setNombre(datos[0]);//nombre
 				clienteAux.setGenero(datos[1]);//genero
 				clienteAux.setEmail(datos[2]);//email
 				clienteAux.setPassword(datos[3]);//contraseņa
 				clienteAux.setSaldo(Double.valueOf(datos[4].replace(",",".")));//saldo
 				hashAux.put(clienteAux.getNombre(), clienteAux);
 
			}		
		} catch (IOException e) {//exception
			
			e.printStackTrace();
		}
		return hashAux;
	}
}

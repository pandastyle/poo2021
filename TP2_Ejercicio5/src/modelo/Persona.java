package modelo;
import java.time.LocalDate;

public class Persona {
	private String nombre;
	private String genero;
	private String email;
	private String password;
	private LocalDate fecha_nacimiento;
	private Double saldo;
	//private static Integer id=1;
	
	public Persona() {
		this("","","",LocalDate.of(1, 1, 1),"contra12",-1.0);
	}

	public Persona(String nombre, String genero, String email, LocalDate fecha_nacimiento, String password, Double saldo) {
		
		this.nombre = nombre;
		this.genero = genero;
		this.email = email;
		this.fecha_nacimiento = fecha_nacimiento;
		this.password= password;
		this.saldo= saldo;
		//id=id+1;
	}
	

	
	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

	/*public static Integer getId() {
		return id;
	}

	public static void setId(Integer id) {
		Persona.id = id;
	}*/

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDate getFecha_nacimiento() {
		return fecha_nacimiento;
	}

	public void setFecha_nacimiento(LocalDate fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}

	@Override
	public String toString() {
		return "nombre=" + nombre + ", genero=" + genero + ", email=" + email + ", password=" + password
				+ ", saldo=" + saldo;
	}
	
	

	
	
	
	
	
}

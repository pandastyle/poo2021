package controlador;

import java.awt.EventQueue;

import vista.VentanaFormulario;

public class Main {
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					new ControladorVentanaFormulario();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}

package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;

import modelo.Banco;
import modelo.Persona;
import vista.VentanaFormulario;

public class ControladorVentanaFormulario implements ActionListener{
	private VentanaFormulario vista;
	private Banco bank;
	
	

	public ControladorVentanaFormulario() {
		
		this.vista = new VentanaFormulario(this);
		this.bank = new Banco();//crea los news
		this.vista.setVisible(true);//se muestrala ventana
	}



	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(getVista().getBtnGuardar())) {//verifica el boton que viene||||entra a la vista y de ahi al boton
			
			//metodo para setear fecha
			Integer dia,mes,anio;
			String[] datosFecha= getVista().getTxtFecha().getText().split("-");
			dia=Integer.valueOf(datosFecha[0]);
			mes=Integer.valueOf(datosFecha[1]);
			anio=Integer.valueOf(datosFecha[2]);
			
			Persona cliente= new Persona(getVista().getTxtNombre().getText(), getVista().
					getTxtGenero().getText(), getVista().getTxtEmail().getText(),LocalDate.of(anio, mes, dia),
					getVista().getTxtContra().getText(),0.0);
			bank.agregarCuenta(cliente);
			bank.listarClientes();
			System.out.println("------------------------------------------");
		}
		
	}



	public VentanaFormulario getVista() {
		return vista;
	}



	public void setVista(VentanaFormulario vista) {
		this.vista = vista;
	}



	public Banco getBank() {
		return bank;
	}



	public void setBank(Banco bank) {
		this.bank = bank;
	}
	
}

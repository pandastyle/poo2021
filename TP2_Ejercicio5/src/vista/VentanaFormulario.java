package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorVentanaFormulario;

import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;

public class VentanaFormulario extends JFrame {

	private JPanel contentPane;
	private JTextField txtNombre;
	private JTextField txtGenero;
	private JTextField txtEmail;
	private JTextField txtContra;
	private JTextField txtFecha;
	private JButton btnGuardar;
	private ControladorVentanaFormulario controlador;

	
	public VentanaFormulario(ControladorVentanaFormulario controlador) {
		this.setControlador(controlador);
		setTitle("Formulario Alta Cliente");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 421, 310);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Nueva Cuenta");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(95, 26, 123, 43);
		contentPane.add(lblNewLabel);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNombre.setBounds(29, 80, 66, 14);
		contentPane.add(lblNombre);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(155, 80, 202, 20);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);
		
		JLabel lblGenero = new JLabel("Genero:");
		lblGenero.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblGenero.setBounds(29, 108, 46, 14);
		contentPane.add(lblGenero);
		
		JLabel lblEmail = new JLabel("Email:");
		lblEmail.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblEmail.setBounds(29, 136, 46, 14);
		contentPane.add(lblEmail);
		
		JLabel lblContra = new JLabel("Contrase\u00F1a:");
		lblContra.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblContra.setBounds(29, 164, 82, 14);
		contentPane.add(lblContra);
		
		JLabel lblFechaNacimiento = new JLabel("Fecha Nacimiento:");
		lblFechaNacimiento.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFechaNacimiento.setBounds(29, 192, 113, 14);
		contentPane.add(lblFechaNacimiento);
		
		txtGenero = new JTextField();
		txtGenero.setColumns(10);
		txtGenero.setBounds(155, 105, 202, 20);
		contentPane.add(txtGenero);
		
		txtEmail = new JTextField();
		txtEmail.setColumns(10);
		txtEmail.setBounds(155, 133, 202, 20);
		contentPane.add(txtEmail);
		
		txtContra = new JTextField();
		txtContra.setColumns(10);
		txtContra.setBounds(155, 161, 202, 20);
		contentPane.add(txtContra);
		
		txtFecha = new JTextField();
		txtFecha.setColumns(10);
		txtFecha.setBounds(155, 189, 202, 20);
		contentPane.add(txtFecha);
		
		btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(getControlador());
		btnGuardar.setBounds(155, 233, 89, 23);
		contentPane.add(btnGuardar);
	}


	public ControladorVentanaFormulario getControlador() {
		return controlador;
	}


	public void setControlador(ControladorVentanaFormulario controlador) {
		this.controlador = controlador;
	}


	public JTextField getTxtNombre() {
		return txtNombre;
	}


	public JTextField getTxtGenero() {
		return txtGenero;
	}


	public JTextField getTxtEmail() {
		return txtEmail;
	}


	public JTextField getTxtContra() {
		return txtContra;
	}


	public JTextField getTxtFecha() {
		return txtFecha;
	}


	public JButton getBtnGuardar() {
		return btnGuardar;
	}


	public void setBtnGuardar(JButton btnGuardar) {
		this.btnGuardar = btnGuardar;
	}
}

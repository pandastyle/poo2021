
/*
 Un bloqueo o abrazo mortal es cuando 2 o m�s hilos 
 est�n esperando por el otro. Ac� tenemos un ejemplo 
 en donde dos amigos tienen por costumbre,
  que si tienen una llamada perdida del otro,
   esperar a que llame nuevamente. 
   Qu� pasa si se llaman al mismo tiempo?*/

public class Ej5_Deadlock {
	static class Amigo {
		private final String nombre;

	

		public synchronized void recibirLlamada(Amigo unAmigo) {
			System.out.format("%s: %s" + "  Me llam�!%n", this.nombre,unAmigo.getNombre());
			unAmigo.devolverLLamada(this);
		}

		public synchronized void devolverLLamada(Amigo unAmigo) {
			System.out.format("%s: %s" + " Me devuelve la llamada!%n", this.nombre, unAmigo.getNombre());
		}	
		public Amigo(String nombre) {
			this.nombre = nombre;
		}

		public String getNombre() {
			return this.nombre;
		}
	}

	public static void main(String[] args) {
		final Amigo pepe = new Amigo("Pepe");
		final Amigo juan = new Amigo("Juan");
		
		new Thread(new Runnable() {
			public void run() {
				pepe.recibirLlamada(juan);
			}
		}).start();
		
		new Thread(new Runnable() {
			public void run() {
				juan.recibirLlamada(pepe);
			}
		}).start();
	}
}
/* Otros posibles problemas:
   Inanici�n:
  	es cuando un hilo no puede tener acceso a los recursos compartidos y eso imposibilita el el progreso de su ejecuci�n.
    Esto pasa cuando otros hilos utilizan constantemente el recurso y no dejan que este sea accedido. 
    Por ejemplo, supongan que un objeto tiene un m�todo sincronizado que toma mucho tiempo en devolver 
    el control y es frecuentemente utilizado, si otro hilo necesita ejecutar el mismo m�todo con la misma frecuencia, 
    este ser� frecuentemente bloqueado. 
 
   Livelock:
    Puede pasar que un hilo act�e dependiendo de lo que hace otro hilo, si la acci�n del otro hilo
 	tambi�n depende de la respuesta del otro hilo, tenemos un livelock. No est�n bloqueados, 
 	simplemente est�n ocupados respondiendo al otro para terminar el trabajo. 
  	Una simple analog�a ser�a la de dos personas tratando de pasarse entre s� en un pasillo,
   	ambos ven que se est�n bloqueando entre s�, Pepe se mueve a la derecha y Juan a la izquierda, 
   	Se siguen bloqueando entre s�, y as� contin�an�
*/
package sincronizacion;

public class Contador {
	private Integer contador = 0;
	
	public synchronized void incrementar() {
		contador++;
	}
	public synchronized void decrementar() {
		contador--;
	}
	public synchronized Integer getContador() {
		return contador;
	}

}

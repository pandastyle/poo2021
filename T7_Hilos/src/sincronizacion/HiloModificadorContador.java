package sincronizacion;

public class HiloModificadorContador implements Runnable {
	private Contador contador;
	

	

	public HiloModificadorContador(Contador contador) {
		super();
		this.contador = contador;
	}

	@Override
	public void run() {
		
		for (int i = 0; i < 100000; i++) {
			getContador().incrementar();
			System.out.println(Thread.currentThread().getName()+": "+ getContador().getContador());
		}
		for (int i = 0; i < 50000; i++) {
			getContador().decrementar();
			System.out.println(Thread.currentThread().getName()+": "+getContador().getContador());
		}
	}
	public Contador getContador() {
		return contador;
	}

}

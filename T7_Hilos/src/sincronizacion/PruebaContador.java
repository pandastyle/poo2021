package sincronizacion;

public class PruebaContador {

	public static void main(String[] args) {
		Contador c = new Contador();
		Thread t1= new Thread(new HiloModificadorContador(c));
		Thread t2= new Thread(new HiloModificadorContador(c));
		
		t1.start();
		t2.start();
		try {
			t1.join();
			t2.join();
			System.out.println("al finalizar tengo: "+c.getContador());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		
	}

}

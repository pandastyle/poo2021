
public class Ej1_HolaRunnable implements Runnable {

	public void run() {
		System.out.println("Hola desde un thread! (con Runnable)");
	}

	public static void main(String args[]) {
		Ej1_HolaRunnable unHolaRunnable = new Ej1_HolaRunnable();
		(new Thread(unHolaRunnable)).start();
	}

}
/*
Esta forma es m�s gen�rica, porque permite heredar de otra
 clase que no sea Thread a diferencia de la otra forma. (ver HelloThread)
 Notar que es necesario crear un Thread y pasar como par�metro la instancia 
 de la clase que implementa la interfaz Runnable
 */

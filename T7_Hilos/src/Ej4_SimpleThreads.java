
/*En este ejemplo se puede ver  c�mo obtener el tiempo de ejecuci�n
  de un hilo y el nombre del Hilo actual, Adem�s m�todos:
 isAlive() : Testea si el Thread est� vivo, se dice
  			 que est� vivo si fue iniciado y aun no termino.
 join(X):     Permite a un hilo esperar a que termine otro,
  			  se puede pasar como par�metro un tiempo l�mite de espera (X). 
  			  Como sleep, el tiempo depende del SO. No hay que asumir 
  			  que va a ser el tiempo exacto que se especifica.  
interrupt(): interrumpe el hilo.
*/
public class Ej4_SimpleThreads {
	
public static void main(String args[]) throws InterruptedException {

		
		//Delay, en milisegundos antes de interrumpir el hilo "mensajeLoop"
		long espera = 1000 * 60 * 60; //1h

	

		imprimir("Comienza thread mensajeLoop");
		long tiempoInicio = System.currentTimeMillis();
		Thread t = new Thread(new MensajeLoop());
		t.start();

		imprimir("Esperando a que Thread MensajeLoop termine");
	
		//Loop hasta que hilo MensajeLoop termine
		while (t.isAlive()) {
			imprimir("Esperando...");
			//Espera maxima de un segundo para que el hilo MensajeLoop termine
			t.join(1000);
			if (((System.currentTimeMillis() - tiempoInicio) > espera) && t.isAlive()) {
				imprimir("Aun esperando!");
				t.interrupt();
			//si supera el tiempo de espera lo interrumpo y espero.
				t.join();
			}
		}
		imprimir("Termino!");
	}


	//El hilo en si..
	private static class MensajeLoop implements Runnable {
		public void run() {
			String importantInfo[] = { "Hola", "Alumnos",
					"de", "POO" };
			try {
				for (int i = 0; i < importantInfo.length; i++) {
					Thread.sleep(4000);
					imprimir(importantInfo[i]);
				}
			} catch (InterruptedException e) {
				imprimir("AUN NO TERMINA!");
			}
		}
	}

	

	// Muestra el nombre del hilo y despu�s un mensaje.
	// Nombre de hilo: Mensaje...
	static void imprimir(String message) {
		String threadName = Thread.currentThread().getName();
		System.out.format("%s: %s%n", threadName, message);
	}

}
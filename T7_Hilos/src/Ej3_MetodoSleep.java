
public class Ej3_MetodoSleep {
	public static void main(String args[]) throws InterruptedException {
		String importantInfo[] = { "Hola", "alumnos", "de", "POO" };

		for (int i = 0; i < importantInfo.length; i++) {
			// pausa de 4 seg
			Thread.sleep(4000);
			// Imprime mensaje
			System.out.println(importantInfo[i]);
			
		}
	}
}
/*
  Ejemplo de sleep, el dormir el hilo permite que el procesador 
  quede disponible para su uso por otro proceso, hay dos sleep sobrecargados
  el 1ero permite especificar el tiempo hasta los milisegundos y el
  otro hasta los nanosegundos. La precisi�n de esto depende del SO. 
  Adem�s el periodo de sleep puede ser terminado por una interrupci�n.
  El main declara que puede tirar la excepci�n InterruptedException,
  esta es necesaria porque el m�todo sleep del hilo actual puede ser 
  interrumpido por otro hilo o proceso, como no hacemos otro hilo que 
  pueda interrumpir no es necesario hacer un catch.
  Notar que no fue necesario hacer una clase que herede de Thread ni nada de eso, 
  simplemente se �durmi� el hilo principal y que esta utilidad sale de un m�todo est�tico de la clase Thread. 
 */
 
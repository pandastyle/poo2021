package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import vista.Vista;

public class Controlador implements KeyListener,ActionListener {
	private Vista vista;
	
	

	public Controlador() {
		vista= new Vista(this);
		vista.setVisible(true);
	}

	private void mostrarInfo(KeyEvent e, String evento) {
		Integer id= e.getID(); // con el id podemos sacar que tipo de evento es 
		String	keyString;
		if (id == KeyEvent.KEY_TYPED) {
			char c = e.getKeyChar();
		keyString = "caracter= '" + c + "'";
		} else {
			Integer keyCode = e.getKeyCode();
			keyString = "codigo= " + keyCode + " ("+ KeyEvent.getKeyText(keyCode)+ ") ";
		}
		
		String modString = "modificador = " + e.getModifiersEx();//identifica el modificador como shift
		String tmpString = KeyEvent.getModifiersExText(e.getModifiersEx());
		
			
		if (tmpString.length()> 0) {
			modString += " ("+ tmpString + ") ";
		} else {
			modString += " (sin modificador)";
		}
		
		
		String actionString = "action? ";
		if (e.isActionKey()) {
			actionString+= "SI";
		}else {
			actionString+= "NO";
		}
		
		String locationString= "Ubicacion: ";
		switch (e.getKeyLocation()) {
		case KeyEvent.KEY_LOCATION_STANDARD:{
			locationString += "Standard";

			break;
		}
		
		case KeyEvent.KEY_LOCATION_LEFT:{
			locationString += "Izquierdo";

			break;
		}
		case KeyEvent.KEY_LOCATION_RIGHT:{
			locationString += "Derecho";

			break;
		}
		case KeyEvent.KEY_LOCATION_NUMPAD:{
			locationString += "NumPad";

			break;
		}
		case KeyEvent.KEY_LOCATION_UNKNOWN:{
			locationString += "desconocido";

			break;
		}}
		

		
		getVista().getTextArea().append(evento + "\n\t"
		+keyString+ "\n\t"
		+modString+ "\n\t"
		+actionString+ "\n\t"
		+locationString+ "\n");
		getVista().getTextArea().setCaretPosition(getVista().getTextArea().getDocument().getLength());
		
	}
	@Override
	public void keyTyped(KeyEvent e) {
		mostrarInfo(e, "KEY TYPED: ");
		
	}

	

	@Override
	public void keyPressed(KeyEvent e) {
		
		mostrarInfo(e, "KEY PRESSED: ");
		
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		getVista().getTextArea().setText("");//limpia el area
		getVista().getTxtinput().setText("");//limpia el txt
		getVista().getTxtinput().requestFocusInWindow();//el componente obtiene el foco
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		mostrarInfo(e, "KEY RELEASED: ");
		
		
	}

	public Vista getVista() {
		return vista;
	}

	public void setVista(Vista vista) {
		this.vista = vista;
	}

	

}

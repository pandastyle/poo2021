
public class Ejemplo_hilos {

	public static void main(String[] args) {
		//long tiempo= 1000*60*60;//expresa una hora
		long tiempo= 5000;
		long inicio= System.currentTimeMillis();
		Thread hilomensaje= new Thread(new Mensaje());//hilo tipo mensaje
		
		hilomensaje.start();
		
		System.out.println("Inicio la ejecucion del hilo");
		while (hilomensaje.isAlive()) {//is alive quiere decir que si el hilo esta vivo 
			imprimir("Esperando...");
			
			try {
				hilomensaje.join(1000);
				if (((System.currentTimeMillis()- inicio) > tiempo) && hilomensaje.isAlive()) {//si el tiempo de ejecucion es mayor 
					imprimir("Ya te esperamos mucho, te interrumpimos");
					hilomensaje.interrupt();
					hilomensaje.join();
				}
			} catch (InterruptedException e) {
			
				e.printStackTrace();
			}
			
		}
		System.out.println("finalizo la ejecucion del hilo");

	}
	

	
	public static void imprimir(String mensaje) {
		
		String nombreHilo= Thread.currentThread().getName();//devuelve el hilo actual con su nombre
		System.out.println("hilo:"+ nombreHilo+ " "+mensaje);
	}

}

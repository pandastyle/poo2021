import java.util.Scanner;

public class Ejercicio_2_Concatenacion {

	public static void main(String[] args) {
		Scanner consola = new Scanner(System.in);
		String cad1,cad2;
		
		System.out.println("Ingrese la primera cadena");
		cad1= consola.nextLine();
		System.out.println("Ingrese la segunda cadena");
		cad2= consola.nextLine();
		System.out.println("El resultado de las 2 cadenas es: "+ cad1+ " "+ cad2);

	}

}

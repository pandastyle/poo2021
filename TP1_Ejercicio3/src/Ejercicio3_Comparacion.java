import java.security.SecureRandom;
import java.util.Iterator;
import java.util.Scanner;

public class Ejercicio3_Comparacion {
public static void main(String[] args) {
	
	Scanner consola = new Scanner(System.in);
	Integer num;
	SecureRandom rnd = new SecureRandom();
	
	Integer numAle=rnd.nextInt(10)+1;
	
	System.out.println("Ingrese un numero para comparar");
	num= consola.nextInt();
	
	if (num>numAle) {
		System.out.println(num+" es mayor que " + numAle);
	}else {
		System.out.println(num+" es menor que " + numAle);
	}
	
	if (num!=numAle) {
		System.out.println(num+" es distinto que " + numAle);
	}else {
		System.out.println(num+" es igual que " + numAle);
	}
	
	if (num>=numAle) {
		System.out.println(num+" es mayor igual que " + numAle);
	}else {
		System.out.println(num+" es menor igual que " + numAle);
	}
	
		
	
}
}

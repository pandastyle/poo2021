
public class Camion extends VehiculoTerrestre {

	private String patenteAcoplado;
	public Camion() {
		super();
		this.setPatenteAcoplado(" ");
		// TODO Auto-generated constructor stub
	}

	public Camion(String marca, String modelo, Float precio, String patente,String patenteAcoplado) {
		super(marca, modelo, precio, patente);
		this.setPatenteAcoplado(patenteAcoplado);
		// TODO Auto-generated constructor stub
	}

	public String getPatenteAcoplado() {
		return patenteAcoplado;
	}

	public void setPatenteAcoplado(String patenteAcoplado) {
		this.patenteAcoplado = patenteAcoplado;
	}
	
	public String infoVehiculo(){
		return super.infoVehiculo()+" "+ this.getPatenteAcoplado();
	}
	
	public String infoVehiculo(String mensaje){
		return this.infoVehiculo()+ " "+ mensaje;
	}
	
	public String arrancar() {
		return "arrancando el camion";
	}

	
}

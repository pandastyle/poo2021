import java.util.ArrayList;
import java.util.Iterator;

public class ApliHerencia {
public static void main(String[] args) {
	
	Auto vento =new Auto("Vento","2020",140000f,"AE345FG");
	Camion scania = new Camion("Scania","2015",234444f,"AB344JB","AC445DC");
	
	System.out.println(vento.getPrecio());
	System.out.println(vento.infoVehiculo());
	System.out.println(scania.infoVehiculo());
	System.out.println("///////////////////////////////////");
	//---------------------------------------------
	
	System.out.println(vento.arrancar());
	System.out.println(scania.arrancar());
	
	/*ArrayList<Object> vehiculos= new ArrayList<Object>();
	
	vehiculos.add(vento);
	vehiculos.add(scania);
	
	for (Object vehiculo : vehiculos) {
		if (vehiculo instanceof Auto) {//si es instancia de
			Auto auto= (Auto)vehiculo;
			auto.arrancar();
		}else if(vehiculo instanceof Camion){
			Camion camion = (Camion)vehiculo;
			camion.arrancar();
		}
	}*/
	ArrayList<VehiculoTerrestre> vehiculos= new ArrayList<VehiculoTerrestre>();
	vehiculos.add(vento);
	vehiculos.add(scania);
	
	/*for (VehiculoTerrestre vehiculo : vehiculos) {
		System.out.println(vehiculo.arrancar());//binding dinamico
	}
	
	System.out.println(scania.infoVehiculo("hola scania"));*/
	
	//---------------------------------------------
	Iterator<VehiculoTerrestre> it= vehiculos.iterator();
	while (it.hasNext()) {
		VehiculoTerrestre vt = (VehiculoTerrestre) it.next();//generalizacion
		System.out.println(vt.arrancar()+" iterator");//binding dinamico
	}
	
}
}

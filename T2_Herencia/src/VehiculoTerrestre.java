
public abstract class VehiculoTerrestre {
	
	private String marca;
	private String modelo;
	private Float precio;
	private String patente;
	
	
	
	public VehiculoTerrestre() {
		this("","",0.0f,"");
	}
	
	
	
	public VehiculoTerrestre(String marca, String modelo, Float precio, String patente) {
		
		this.marca = marca;
		this.modelo = modelo;
		this.precio = precio;
		this.patente = patente;
	}



	protected String getMarca() {
		return marca;
	}
	protected void setMarca(String marca) {
		this.marca = marca;
	}
	protected String getModelo() {
		return modelo;
	}
	protected void setModelo(String modelo) {
		this.modelo = modelo;
	}
	protected Float getPrecio() {
		return precio;
	}
	protected void setPrecio(Float precio) {
		this.precio = precio;
	}
	protected String getPatente() {
		return patente;
	}
	protected void setPatente(String patente) {
		this.patente = patente;
	}
	
	public String infoVehiculo() {
		return this.getMarca() + " "+ this.getModelo()+ " "+ this.getPrecio()+" "+ this.getPatente();
	}
	
	public abstract String arrancar();// metodo abstracto - polimorfismo reemplazar el hijo por el padre
	
}

import java.util.Iterator;
import java.util.Random;

public class Productor implements Runnable {
	private Compartido compartido;
	
	public Productor(Compartido compartido) {
		super();
		this.compartido = compartido;
	}

	@Override
	public void run() {
		Random ran = new Random();
		for (int i = 0; i < ran.nextInt(5)+1; i++) {
			Integer num= ran.nextInt();
			getCompartido().insertar(num);
			System.out.println(Thread.currentThread().getName()+": inserto:"+ num);
		}
		
		getCompartido().insertar(null);
		System.out.println("Finaliza: "+Thread.currentThread().getName());
		
	}

	public Compartido getCompartido() {
		return compartido;
	}

	public void setCompartido(Compartido compartido) {
		this.compartido = compartido;
	}
	
}

import java.util.ArrayList;
import java.util.LinkedList;

public class Compartido {
	private LinkedList<Integer> numeros;
	
	

	public Compartido() {
		super();
		this.numeros = new LinkedList<Integer>();
	}

	public synchronized void insertar(Integer num) {
	 numeros.add(num);	
	 notifyAll();//notifica a todos para que se despierten
	}
	
	public synchronized Integer retirar() {
		while (getNumeros().isEmpty()) {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
			}
		return this.getNumeros().removeLast();
		
	}
	
	public LinkedList<Integer> getNumeros() {
		return numeros;
	}

	public void setNumeros(LinkedList<Integer> numeros) {
		this.numeros = numeros;
	}
	
}


public class MainProductorConsumidor {

	public static void main(String[] args) {
		Compartido compartido = new Compartido();
		Thread t1= new Thread(new Productor(compartido));
		t1.setName("PRODUCTOR 1");	
		t1.start();
		Thread t2= new Thread(new Productor(compartido));
		t2.setName("PRODUCTOR 2");	
		t2.start();
		
		
		Thread t4= new Thread(new Consumidor(compartido));
		t4.setName("CONSUMIDOR 1");
		t4.start();

	}

}

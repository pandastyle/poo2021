import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Ejercicio6_Loteria {

	public static void main(String[] args) {
		Scanner consola = new Scanner(System.in);
		SecureRandom rnd = new SecureRandom();
		Integer numAle,numUsu;
		Boolean rta,respuesta;
		
		
		respuesta=true;
		while(respuesta) {
	    Integer aciertos=0;
	    ArrayList<Integer> loteria= new ArrayList<>();
		ArrayList<Integer> lotUsuario= new ArrayList<>();
		/////////////////////////generacion numeros ganadores///////////////////////////
		for (int i = 1; i <=6; i++) {
			rta=true;
			numAle= rnd.nextInt(50)+1;
			while(rta) {
				if(loteria.contains(numAle)) {
					numAle= rnd.nextInt(50)+1;
				}else {
					loteria.add(numAle);
					rta= false;
				}
			}
		}
		
		/////////////////////////eleccion numeros loteria///////////////////////////
		System.out.println("Elige 6 numeros del 1 al 50");
		
		for (int i = 1; i <= 6; i++) {
			rta=true;
			while(rta) {
			System.out.print("Numero "+ i+": ");
			numUsu= consola.nextInt();
			
			if (lotUsuario.contains(numUsu)) {// contains sirve para buscar un numero en el arraylist
				System.out.println("Ingrese otro Numero");
			}else {
				lotUsuario.add(numUsu);
				rta=false;
			}
			}
			
		}
		
		
		//////////////////////acomodado y mostrado/////////////////////
		Collections.sort(loteria);//ordena el arraylist de menor a mayor
		Collections.sort(lotUsuario);
		
		System.out.print("Numeros Ganadores: ");
		for (Integer numGanadores : loteria) {
			System.out.print(numGanadores+" ");
		}
		System.out.println("");
		
		System.out.print("Numeros elegidos: ");
		for (Integer numElegidos : lotUsuario) {
			System.out.print(numElegidos+" ");
		}
		System.out.println("");
		
		////////////////////////////ACIERTOS////////////////////////////
		
		for (Integer numElegidos : lotUsuario) {
			if (loteria.contains(numElegidos)) {
				aciertos++;
			}
		}
		System.out.println("ACIERTOS OBTENIDOS: "+ aciertos);
		System.out.println("Jugar denuevo?si/no");
		if (consola.next().equalsIgnoreCase("no")) {
			respuesta=false;
		}
		System.out.println("--------------------------------");
		}

	}

}
	

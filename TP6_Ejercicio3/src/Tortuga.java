
public class Tortuga {
	private Integer casilla;

	public Tortuga() {
		
		this.casilla = 1;
	}
	
	public void avanceRapido() {
		this.setCasilla(getCasilla()+3);
		if (this.getCasilla()>70) {
			this.setCasilla(70);
		}
	}
	
	public void resbalo() {
		this.setCasilla(getCasilla()-1);
		if (this.getCasilla()<1) {
			this.setCasilla(1);
		}
	}
	
	public void avance() {
		this.setCasilla(getCasilla()+1);
		if (this.getCasilla()>70) {
			this.setCasilla(70);
		}
	}

	public Integer getCasilla() {
		return casilla;
	}

	public void setCasilla(Integer casilla) {
		this.casilla = casilla;
	}
	
	
}

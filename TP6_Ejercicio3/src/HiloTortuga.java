import java.security.SecureRandom;

public class HiloTortuga implements Runnable {
	private Tortuga tortu;
	
	public HiloTortuga(Tortuga tortu) {
		super();
		this.tortu = tortu;
	}

	@Override
	public void run() {
		while (this.getTortu().getCasilla()!=70) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			SecureRandom ran= new SecureRandom();
			Integer num= ran.nextInt(100)+1;
			
			if (num>0 && num<=50) {
				this.getTortu().avanceRapido();
			}
			if (num>50 && num<=70) {
				this.getTortu().resbalo();
			}
			if (num>70 && num<=100) {
				this.getTortu().avance();
			}
			
			System.out.println("Tortuga en posicion "+ this.getTortu().getCasilla() );
			
		}

	}

	public Tortuga getTortu() {
		return tortu;
	}

	public void setTortu(Tortuga tortu) {
		this.tortu = tortu;
	}
	

}

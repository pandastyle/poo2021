import java.security.SecureRandom;

public class HiloLiebre implements Runnable {
	private Liebre liebre;
	
	public HiloLiebre(Liebre liebre) {
		super();
		this.liebre = liebre;
	}
	
	@Override
	public void run() {
		while (this.getLiebre().getCasilla()!=70) {
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			SecureRandom ran= new SecureRandom();
			Integer num= ran.nextInt(100)+1;
			
			 
			if (num>=1 && num<=20) {
				this.getLiebre().duerme();
			}
			if (num>20 && num<=40) {
				this.getLiebre().granSalto();
			}
			if (num>40 && num<=50) {
				this.getLiebre().resbalonGrande();
			}
			if (num>50 && num<=80) {
				this.getLiebre().peque�oSalto();
			}
			if (num>80 && num<=100) {
				this.getLiebre().resbalonPeque�o();
			}
			System.out.println("Liebre en posicion "+ this.getLiebre().getCasilla());
			
		}

	}

	public Liebre getLiebre() {
		return liebre;
	}

	public void setLiebre(Liebre liebre) {
		this.liebre = liebre;
	}
	


	

}

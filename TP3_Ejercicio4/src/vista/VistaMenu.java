package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorVM;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

public class VistaMenu extends JFrame {

	private JPanel contentPane;
	private JComboBox companiasBox;
	private JTextField txtMensajes;
	private JTextField txtMin;
	private JLabel lblMinutos;
	private JTextField txtGB;
	private JLabel lblGB;
	private JButton btnCalcular;
	private ControladorVM controlador;

	/**
	 * Launch the application.
	 */
	
	/**
	 * Create the frame.
	 */
	public VistaMenu(ControladorVM controlador) {
		this.setControlador(controlador);
		setTitle("Calculadora de Tarifa");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 428, 236);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		String[] companias= {"Claro","Personal","Movistar"};
		companiasBox = new JComboBox(companias);
		companiasBox.setBounds(49, 45, 307, 22);
		contentPane.add(companiasBox);
		
		JLabel lblCompanias = new JLabel("Seleccione Compa\u00F1ia:");
		lblCompanias.setBounds(49, 24, 135, 14);
		contentPane.add(lblCompanias);
		
		txtMensajes = new JTextField();
		txtMensajes.setBounds(53, 107, 86, 20);
		contentPane.add(txtMensajes);
		txtMensajes.setColumns(10);
		
		JLabel lblMensajes = new JLabel("Cantidad Mensajes:");
		lblMensajes.setBounds(44, 88, 116, 14);
		contentPane.add(lblMensajes);
		
		txtMin = new JTextField();
		txtMin.setColumns(10);
		txtMin.setBounds(167, 107, 86, 20);
		contentPane.add(txtMin);
		
		lblMinutos = new JLabel("Cantidad Minutos:");
		lblMinutos.setBounds(167, 88, 116, 14);
		contentPane.add(lblMinutos);
		
		txtGB = new JTextField();
		txtGB.setColumns(10);
		txtGB.setBounds(285, 107, 86, 20);
		contentPane.add(txtGB);
		
		lblGB = new JLabel("GBs Consumidos:");
		lblGB.setBounds(287, 88, 116, 14);
		contentPane.add(lblGB);
		
		btnCalcular = new JButton("Calcular Tarifa");
		btnCalcular.setBounds(151, 153, 119, 22);
		btnCalcular.addActionListener(getControlador());
		contentPane.add(btnCalcular);
		this.setLocationRelativeTo(null);
	}

	public JComboBox getCompaniasBox() {
		return companiasBox;
	}

	public JTextField getTxtMensajes() {
		return txtMensajes;
	}

	public JTextField getTxtMin() {
		return txtMin;
	}

	public JTextField getTxtGB() {
		return txtGB;
	}

	public JButton getBtnCalcular() {
		return btnCalcular;
	}

	public ControladorVM getControlador() {
		return controlador;
	}

	public void setControlador(ControladorVM controlador) {
		this.controlador = controlador;
	}
	
}

package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import modelo.Claro;
import modelo.CompaniaTelefonica;
import modelo.Movistar;
import modelo.Personal;
import vista.VistaMenu;

public class ControladorVM implements ActionListener {
	private VistaMenu vista;
	private CompaniaTelefonica companias;
	
	
	
	public ControladorVM() {
		
		this.setVista(new VistaMenu(this));
		this.getVista().setVisible(true);
	}



	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(getVista().getBtnCalcular())) {
			String compania= (String)getVista().getCompaniasBox().getSelectedItem();
			Integer mensaje=Integer.valueOf(getVista().getTxtMensajes().getText());
			Integer minutos= Integer.valueOf(getVista().getTxtMin().getText());
			Integer gbs= Integer.valueOf(getVista().getTxtGB().getText());
			
			switch (compania) {
			case "Claro": {
				companias= new Claro(mensaje,minutos,gbs);
				break;
			}
			case "Personal": {
				companias= new Personal(mensaje,minutos,gbs);
				break;
			}
			case "Movistar": {
				companias= new Movistar(mensaje,minutos,gbs);
			}
			
			default:
				throw new IllegalArgumentException("Unexpected value: " + compania);
			}
			
			JOptionPane.showMessageDialog(null,"El Monto Total es: $"+ companias.calcularTarifa());
			
			
		}
		
	}



	public VistaMenu getVista() {
		return vista;
	}



	public void setVista(VistaMenu vista) {
		this.vista = vista;
	}

}

package controlador;

import java.awt.EventQueue;

import vista.VistaMenu;

public class Main {
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					new ControladorVM();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}

package modelo;

public abstract class CompaniaTelefonica {
   private Integer cantMensajes;
   private Integer minLlamadas;
   private Integer consumoGB;
   private Double montoTotal;
   
   
   
   
   
		public CompaniaTelefonica(Integer cantMensajes, Integer minLlamadas, Integer consumoGB) {
			
			this.setCantMensajes(cantMensajes);//mensaje cuesta 1
			this.setConsumoGB(consumoGB*20);//GB cuesta 20
			this.setMinLlamadas(minLlamadas*15);//min cuesta 15
			
		}
		
		
		public Integer getCantMensajes() {
			return cantMensajes;
		}
		public void setCantMensajes(Integer cantMensajes) {
			this.cantMensajes = cantMensajes;
		}
		public Integer getMinLlamadas() {
			return minLlamadas;
		}
		public void setMinLlamadas(Integer minLlamadas) {
			this.minLlamadas = minLlamadas;
		}
		public Integer getConsumoGB() {
			return consumoGB;
		}
		public void setConsumoGB(Integer consumoGB) {
			this.consumoGB = consumoGB;
		}


		public Double getMontoTotal() {
			return montoTotal;
		}


		public void setMontoTotal(Double montoTotal) {
			this.montoTotal = montoTotal;
		}
		
		public  Double calcularTarifa() {
			Double total= (double) (getCantMensajes()+getConsumoGB()+getMinLlamadas());
			setMontoTotal(total);
			return montoTotal;
		}
		
		   
   
}

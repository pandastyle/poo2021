package modelo;

public class Movistar extends CompaniaTelefonica {

	public Movistar(Integer cantMensajes, Integer minLlamadas, Integer consumoGB) {
		super(cantMensajes, minLlamadas, consumoGB);
		
	}

	@Override
	public Double calcularTarifa() {
		Double extraMensajes=(Double)(getCantMensajes()*0.10);// extra 10% mensajes
		Double extraMinutos= (Double)(getMinLlamadas()*0.20);//extra 20% min
		Double extraGB= (Double)(getConsumoGB()*0.30);//extra 30%GB
		Double totalMin= getMinLlamadas()+extraMinutos;
		Double totalGB= getConsumoGB()+extraGB;
		Double totalMen= getCantMensajes()+extraMensajes;
		
		setMontoTotal(totalMen+totalMin+totalGB);

		return getMontoTotal();
	}

}

package modelo;

public class Personal extends CompaniaTelefonica {

	public Personal(Integer cantMensajes, Integer minLlamadas, Integer consumoGB) {
		super(cantMensajes, minLlamadas, consumoGB);
	
	}

	@Override
	public Double calcularTarifa() {
		Double extraMinutos= (Double)(getMinLlamadas()*0.20);//extra 20% min
		Double extraGB= (Double)(getConsumoGB()*0.50);//extra 50%GB
		Double totalMin= getMinLlamadas()+extraMinutos;
		Double totalGB= getConsumoGB()+extraGB;
		
		setMontoTotal(getCantMensajes()+totalMin+totalGB);
		return getMontoTotal();
	}

}

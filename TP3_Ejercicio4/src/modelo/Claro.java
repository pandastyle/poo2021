package modelo;

public class Claro extends CompaniaTelefonica {

	public Claro(Integer cantMensajes, Integer minLlamadas, Integer consumoGB) {
		super(cantMensajes, minLlamadas, consumoGB);
		
	}

	@Override
	public Double calcularTarifa() {
		setMontoTotal((double)(getCantMensajes()+getMinLlamadas()+getConsumoGB()));
		Double extraTotal= getMontoTotal()*0.20;
		
		return getMontoTotal()+extraTotal;//devuelve el monto total agregado con el 20% extra
	}

}

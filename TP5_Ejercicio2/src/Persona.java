
public class Persona {
	private String nombre;
	private Integer edad;
	
	
	public Persona() {
		
	}

	public Persona(String nombre, Integer edad) {
		super();
		this.nombre = nombre;
		this.edad = edad;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Integer getEdad() {
		return edad;
	}
	public void setEdad(Integer edad) throws EdadFueraDeRANGO{
		if ((edad>=0)&&(edad<=100)) {
		this.edad = edad;
	}else {
		throw new EdadFueraDeRANGO("Edad fuera de Rango");
	}
	}
	
	
}

package modelo;

import java.util.ArrayList;

public class Agenda {
	private ArrayList<Persona> agenda = new ArrayList<Persona>();

	public ArrayList<Persona> getAgenda() {
		return agenda;
	}

	public void setAgenda(ArrayList<Persona> agenda) {
		this.agenda = agenda;
	}
	
	public Boolean agregarPersona(Persona persona) {
		
		return this.agenda.add(persona);
	}
	
	
	public String toString() {
	String cadena= "Nombre\tApellido\tTelefono\n";
		for (Persona persona : agenda) {
			cadena = cadena + persona.getNombre() + "\t"+ persona.getApellido()+ "\t\t"+ persona.getTelefono()+ "\n";
		}
		return cadena;
	}

}

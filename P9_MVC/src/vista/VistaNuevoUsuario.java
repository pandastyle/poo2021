package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorAgenda;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.FlowLayout;
import javax.swing.JTextField;
import java.awt.Component;
import javax.swing.Box;
import java.awt.Dimension;

public class VistaNuevoUsuario extends JFrame {

	private JPanel contentPane;
	private JButton btnAceptar;
	private JButton btnCancelar;
	private JTextField txtNombre;
	private JTextField txtApellido;
	private JTextField txtTelefono;
	private ControladorAgenda controlador;
	private JLabel errorNombre;
	private JLabel errorApellido;
	private JLabel errorTelefono;

	
	public VistaNuevoUsuario(ControladorAgenda controlador) {
		this.setControlador(controlador);//se setea el controlador
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 323, 328);
		contentPane = new JPanel();
		contentPane.setBackground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JLabel lblNewLabel = new JLabel("Nuevo Contacto");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 15));
		contentPane.add(lblNewLabel, BorderLayout.NORTH);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.BLACK);
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setAlignment(FlowLayout.RIGHT);
		contentPane.add(panel, BorderLayout.SOUTH);
		
		btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(this.getControlador());//se le dice el controlador
		panel.add(btnAceptar);
		
		btnCancelar = new JButton("Cancelar");
		panel.add(btnCancelar);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		contentPane.add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(null);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(16, 28, 71, 14);
		panel_1.add(lblNombre);
		
		txtNombre = new JTextField();
		lblNombre.setLabelFor(txtNombre);
		txtNombre.setBounds(97, 25, 150, 20);
		panel_1.add(txtNombre);
		txtNombre.addFocusListener(controlador);
		txtNombre.setColumns(10);
		
		JLabel lblApellido = new JLabel("Apellido: ");
		lblApellido.setBounds(16, 75, 59, 14);
		panel_1.add(lblApellido);
		
		txtApellido = new JTextField();
		txtApellido.addFocusListener(controlador);
		txtApellido.setColumns(10);
		txtApellido.setBounds(97, 72, 150, 20);
		panel_1.add(txtApellido);
		
		JLabel lblTelefono = new JLabel("Telefono:");
		lblTelefono.setBounds(16, 128, 59, 14);
		panel_1.add(lblTelefono);
		
		txtTelefono = new JTextField();
		txtTelefono.setColumns(10);
		txtTelefono.setBounds(97, 125, 150, 20);
		txtTelefono.addFocusListener(controlador);
		panel_1.add(txtTelefono);
		
		errorNombre = new JLabel("Ingrese un nombre porfavor");
		errorNombre.setForeground(Color.RED);
		errorNombre.setBounds(68, 53, 159, 14);
		panel_1.add(errorNombre);
		
		errorApellido = new JLabel("Ingrese un apellido porfavor");
		errorApellido.setForeground(Color.RED);
		errorApellido.setBounds(68, 100, 159, 14);
		panel_1.add(errorApellido);
		
		errorTelefono = new JLabel("Ingrese un telefono porfavor");
		errorTelefono.setForeground(Color.RED);
		errorTelefono.setBounds(68, 156, 159, 14);
		panel_1.add(errorTelefono);
		
		Component rigidArea = Box.createRigidArea(new Dimension(20, 20));
		contentPane.add(rigidArea, BorderLayout.WEST);
		
		Component rigidArea_1 = Box.createRigidArea(new Dimension(20, 20));
		contentPane.add(rigidArea_1, BorderLayout.EAST);
		
		this.getErrorNombre().setVisible(false);
		this.getErrorApellido().setVisible(false);
		this.getErrorTelefono().setVisible(false);
	
	}

	public JButton getBtnAceptar() {
		return btnAceptar;
	}

	public void setBtnAceptar(JButton btnAceptar) {
		this.btnAceptar = btnAceptar;
	}

	public JButton getBtnCancelar() {
		return btnCancelar;
	}

	public void setBtnCancelar(JButton btnCancelar) {
		this.btnCancelar = btnCancelar;
	}

	public ControladorAgenda getControlador() {
		return controlador;
	}

	public void setControlador(ControladorAgenda controlador) {
		this.controlador = controlador;
	}

	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public JTextField getTxtApellido() {
		return txtApellido;
	}

	public JTextField getTxtTelefono() {
		return txtTelefono;
	}

	public JLabel getErrorNombre() {
		return errorNombre;
	}

	public void setErrorNombre(JLabel errorNombre) {
		this.errorNombre = errorNombre;
	}

	public JLabel getErrorApellido() {
		return errorApellido;
	}

	public void setErrorApellido(JLabel errorApellido) {
		this.errorApellido = errorApellido;
	}

	public JLabel getErrorTelefono() {
		return errorTelefono;
	}

	public void setErrorTelefono(JLabel errorTelefono) {
		this.errorTelefono = errorTelefono;
	}
}

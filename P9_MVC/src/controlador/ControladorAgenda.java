package controlador;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.border.LineBorder;

import modelo.Agenda;
import modelo.Persona;
import vista.VistaNuevoUsuario;

public class ControladorAgenda implements ActionListener, FocusListener{
  private VistaNuevoUsuario vista;//crear vista
  private Agenda agenda;//crear modelo
  
	public ControladorAgenda() {
	super();
	this.vista = new VistaNuevoUsuario(this);//necesita intancia del controlador
	this.agenda = new Agenda();
	this.vista.setVisible(true);
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(getVista().getBtnAceptar())) {//verifica el boton que viene
			String nombre= getVista().getTxtNombre().getText();
			String apellido= getVista().getTxtApellido().getText();
			String telefono= getVista().getTxtTelefono().getText();
			
			getAgenda().agregarPersona(new Persona(nombre, apellido, telefono));//agrega la persona a la agenda
			
			System.out.println(getAgenda().toString());
		}
		
	}
	
	
	public VistaNuevoUsuario getVista() {
		return vista;
	}
	public void setVista(VistaNuevoUsuario vista) {
		this.vista = vista;
	}
	public Agenda getAgenda() {
		return agenda;
	}
	public void setAgenda(Agenda agenda) {
		this.agenda = agenda;
	}


	@Override
	public void focusGained(FocusEvent e) {//se gana el foco
		if (e.getSource().equals(getVista().getTxtNombre())) {
			this.getVista().getTxtNombre().setBorder(new LineBorder(Color.BLACK));//se pone el borde en negro
		}
		
	}


	@Override
	public void focusLost(FocusEvent e) {//se pierde el foco
		if (e.getSource().equals(getVista().getTxtNombre())) {
			if (getVista().getTxtNombre().getText().equals("")) {
			this.getVista().getTxtNombre().setBorder(new LineBorder(Color.RED));//se pone el borde en rojo
			this.getVista().getErrorNombre().setVisible(true);
			}else
			{
				this.getVista().getTxtNombre().setBorder(new LineBorder(Color.GREEN));//se pone el borde en verde
				this.getVista().getErrorNombre().setVisible(false);
			}
		}
		
	}
	

}

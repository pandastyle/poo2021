
public class Carrera {
		private String codigo;
		private String nombre;
		private String departamento;
		private Integer cantMaterias;
		
		public Carrera() {
			this("","","",0);
		}
		
		public Carrera(String codigo, String nombre, String departamento, Integer cantMaterias) {
			super();
			this.codigo = codigo;
			this.nombre = nombre;
			this.departamento = departamento;
			this.cantMaterias = cantMaterias;
		}

		public String getCodigo() {
			return codigo;
		}

		public void setCodigo(String codigo) {
			this.codigo = codigo;
		}

		public String getNombre() {
			return nombre;
		}

		public void setNombre(String nombre) {
			this.nombre = nombre;
		}

		public String getDepartamento() {
			return departamento;
		}

		public void setDepartamento(String departamento) {
			this.departamento = departamento;
		}

		public Integer getCantMaterias() {
			return cantMaterias;
		}

		public void setCantMaterias(Integer cantMaterias) {
			this.cantMaterias = cantMaterias;
		}
		
		public String InfoCarrera() {
			return this.getCodigo()+ " " +this.getNombre()+ " "+ this.getDepartamento()+ " "+ this.getCantMaterias();
		}
		
}

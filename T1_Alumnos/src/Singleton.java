
public class Singleton {
	
	private static Singleton  instancia=null;
	
	private Singleton() {
		
	}
	
	public static Singleton obtenerInstancia() {
		if (instancia==null) {
			instancia= new Singleton();
		}
		return instancia;
	}
	
	
}

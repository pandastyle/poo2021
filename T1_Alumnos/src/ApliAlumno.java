import java.util.ArrayList;

public class ApliAlumno {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Carrera informatica= new Carrera("034","Lic.Informatica", "Informatica",34);
    Alumno martin= new Alumno();
    Alumno juan= new Alumno("Juan","Perez",38,informatica);
    
    juan.setEdad(juan.getEdad()+1);
    martin.setApellido("Bilbao");
    martin.setNombre("Martin");
    martin.setEdad(39);
    
    System.out.println(juan.getEdad());
    System.out.println(martin.getEdad());
    System.out.println(martin.getNombre() + " " + martin.getApellido());
   // Alumno.setValor("Nike");// si es estatico lo setea la clase
    System.out.println(martin.Informacion());
    
    //Float nota1= new Float(3);
    //System.out.println(martin.promediarNotas(nota1, 9.0f, 6.0f));
    
    Profesor pancho= new Profesor();
    pancho.setNombre("Francisco");
    System.out.println(pancho.promediarNotas(3.5f, new Float(8), 6.7f));
    
    String uno= "martin";
    String dos = "pablo";
    System.out.println(Integer.toBinaryString(7));
    System.out.println(uno.equals(dos));
    System.out.println(martin.equals(juan));
    System.out.println("-------------------");
    
    Direccion dirPancho= new Direccion();
   /* dirPancho.setCalle("Rivadavia");
    dirPancho.setCiudad("CR");
    dirPancho.setNumero(243);*/
    
    pancho.getDireccion().setCalle("rivadavia");
    pancho.getDireccion().setCiudad("CR");
    pancho.getDireccion().setNumero(243);
    //pancho.setDireccion(dirPancho);
    System.out.println(pancho.getDireccion().getCalle());
    System.out.println("-------------------");
    ///////////////////////////////////////////////////////
    Facultad ingenieria= new Facultad("Ingenieria","CR",new ArrayList<Alumno>(),new ArrayList<Carrera>());
    ingenieria.agregarAlumno(martin);
    ingenieria.agregarAlumno(juan);
    System.out.println(ingenieria.imprimirAlumnos());
    Alumno pedro= new Alumno("Pedro","Perez",78,informatica);
    ingenieria.agregarAlumno(pedro);
    ingenieria.agregarCarrera(informatica);
    System.out.println(ingenieria.imprimirAlumnos());
    martin.setCarrera(informatica);
    System.out.println(ingenieria.imprimirAlumnos());
    //////////////////////////////////////////////////
    //SINGLETON//SINGLETON//SINGLETON//SINGLETON//SINGLETON 12-08-21 CLASE 4
    System.out.println("---------------SINGLETON------------------");
    System.out.println(martin.toString());
    System.out.println(juan.toString());
    System.out.println(pedro.toString());
    
    Singleton uno1 = Singleton.obtenerInstancia();
    Singleton dos1 = Singleton.obtenerInstancia();
    System.out.println(uno1.toString());
    System.out.println(dos1.toString());
    
    
    
	}

}

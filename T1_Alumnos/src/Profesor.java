
public class Profesor {
	private String nombre;
	private Direccion direccion;// agregacion

	
	public Profesor() {
		this("",new Direccion());
	}

	public Profesor(String nombre, Direccion direccion) {
		
		this.setNombre(nombre);
		this.setDireccion(direccion);
	}

	
	

	public Direccion getDireccion() {
		return direccion;
	}




	public void setDireccion(Direccion direccion) {
		this.direccion = direccion;
	}




	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public Float promediarNotas(Float uno, Float dos, Float tres) {
		// Float promedio=(uno+dos+tres)/3.0f;
		// return promedio;
			return (uno+dos+tres)/3.0f;

		}
	
}

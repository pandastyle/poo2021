import java.util.ArrayList;

public class Alumno {
	//atributos de instancia
	private String nombre;
	private String apellido;
	private Integer edad;
	private Carrera carrera;
	
	//atributo de clase 
	//private static String valor;
	//constructor
	public Alumno() { //por defecto
		//this busca metodo de esta clase
		this("","",-1, new Carrera());//ahorrando lineas
		/*this.setNombre("");
		this.setApellido("");
		this.setEdad(-1);*/
	}
	
	public Alumno(String nombre, String apellido, Integer edad,Carrera carrera) {//Constructor sobrecargado
		this.setNombre(nombre);
		this.setApellido(apellido);
		this.setEdad(edad);
		this.setCarrera(carrera);
		
	}

	public Carrera getCarrera() {
		return carrera;
	}

	public void setCarrera(Carrera carrera) {
		this.carrera = carrera;
	}

	public String getNombre() {//Getter
		return nombre;
	}

	public void setNombre(String nombre) {//Setter
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}

	/*public static String getValor() {
		return valor;
	}

	public static void setValor(String valor) {
		Alumno.valor = valor;
	}*/
	
	public String Informacion() {
		return "Nombre: "+ this.getNombre()+" Apellido: "+ this.getApellido()+" Edad: "+ this.getEdad()+" "+this.getCarrera().InfoCarrera();
	}
	
	/*public Float promediarNotas(Float uno, Float dos, Float tres) {
	// Float promedio=(uno+dos+tres)/3.0f;
	// return promedio;
		return (uno+dos+tres)/3.0f;

	}*/
	
	
	
	
}

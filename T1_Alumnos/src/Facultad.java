import java.util.ArrayList;

public class Facultad {
 private String nombre;
 private String sede;
 private ArrayList<Alumno> alumnos;
 private ArrayList<Carrera> carreras;
 
		 public Facultad() {
			this("","",new ArrayList<Alumno>(),new ArrayList<Carrera>());
		 }

	public Facultad(String nombre, String sede, ArrayList<Alumno> alumnos,ArrayList<Carrera> carreras) {
		super();
		this.nombre = nombre;
		this.sede = sede;
		this.alumnos = alumnos;
		this.carreras= carreras;
	}

		public ArrayList<Carrera> getCarreras() {
		return carreras;
	}

	public void setCarreras(ArrayList<Carrera> carreras) {
		this.carreras = carreras;
	}

		public String getNombre() {
			return nombre;
		}
		
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		
		public String getSede() {
			return sede;
		}
		
		public void setSede(String sede) {
			this.sede = sede;
		}
		
		public ArrayList<Alumno> getAlumnos() {
			return alumnos;
		}
		
		public void setAlumnos(ArrayList<Alumno> alumnos) {
			this.alumnos = alumnos;
		}
		 
		public void agregarAlumno(Alumno alumno) {
			this.getAlumnos().add(alumno);
		}
		
		public void agregarCarrera(Carrera carrera) {
			this.getCarreras().add(carrera);
		}
		
		public String imprimirAlumnos() {
			String mensaje="";
			for (Alumno alumno : alumnos) {
				mensaje= mensaje.concat(alumno.Informacion()+" | ");
			}
			return mensaje;
			
		}
 
 
 
}

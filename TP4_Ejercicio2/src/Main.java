import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class Main {

	public static void main(String[] args) {
		Scanner consola= new Scanner(System.in);
		try {
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("sonido/sonido.wav").getAbsoluteFile());
			Clip sonido= AudioSystem.getClip();
			sonido.open(audioInputStream);
			
		Integer seleccion;
		System.out.println("Seleccione una opcion");
		System.out.printf("1. Reproduce el sonido una sola vez\n"
				        + "2. Reproduce el sonido dos veces\n"
				        + "3. Reproduce el sonido N veces\n"
				        + "4. Reproduce el sonido en un bucle infinito\n");
		
		seleccion= consola.nextInt();
		switch (seleccion) {
		case 1: {
			
			sonido.loop(1);
			
			break;
		}
		case 2: {
			sonido.loop(2);	
			break;
				}
		case 3: {
			System.out.println("Ingrese la cantidad de veces");
			Integer cantidad= consola.nextInt();
			sonido.loop(cantidad);
			break;
		}
		case 4: {
			sonido.loop(99);
			break;
		}
		default:
			throw new IllegalArgumentException("Unexpected value: " + seleccion);
			
		}seleccion= consola.nextInt();
		} catch (LineUnavailableException | UnsupportedAudioFileException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}

import java.time.LocalDate;
import java.util.ArrayList;

public class Main {
public static void main(String[] args) {
	Persona per= new Persona("Dylan Salazar", "M", "gmdylanuni@hotmail.com",
			LocalDate.of(1999, 11, 27), "contra123", new ArrayList<VideoJuego>());
	
	VideoJuego juego1= new VideoJuego("pacman", "1980", "Namco", Genero.ARCADE);
	per.agregarJuego(juego1);
	VideoJuego juego2= new VideoJuego("Mario Bros", "1985", "Nintendo", Genero.AVENTURA);
	per.agregarJuego(juego2);
	
	ArrayList<VideoJuego> recorrer= per.getColecjuego();
	System.out.println( per.toString());
	for (VideoJuego videoJuego : recorrer) {
		System.out.println(videoJuego.InfoJuego());
	}
	
	
	VideoJuego prueba= new VideoJuego();
	System.out.println(prueba.InfoJuego());
	
}
}

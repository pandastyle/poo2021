import java.time.LocalDate;
import java.util.ArrayList;

public class Persona {
	private String nombre;
	private String genero;
	private String email;
	private String password;
	private LocalDate fecha_nacimiento;
	private ArrayList<VideoJuego> colecjuego;
	
	public Persona() {
		this("","","",LocalDate.of(1, 1, 1),"contra12", new ArrayList<VideoJuego>());
	}

	public Persona(String nombre, String genero, String email, LocalDate fecha_nacimiento, String password,ArrayList<VideoJuego> colecJuego) {
		
		this.setNombre(nombre);
		this.setGenero(genero);
		this.setEmail(email);
		this.setPassword(password);
		this.setFecha_nacimiento(fecha_nacimiento);
		this.setColecjuego(colecJuego);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public LocalDate getFecha_nacimiento() {
		return fecha_nacimiento;
	}

	public void setFecha_nacimiento(LocalDate fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}

	public ArrayList<VideoJuego> getColecjuego() {
		return colecjuego;
	}

	public void setColecjuego(ArrayList<VideoJuego> colecjuego) {
		this.colecjuego = colecjuego;
	}

	
	public String toString() {
		return "Persona [nombre=" + nombre + ", genero=" + genero + ", email=" + email + ", password=" + password
				+ ", fecha_nacimiento=" + fecha_nacimiento + ", colecjuego=" + colecjuego + "]";
	}
	
	public void agregarJuego(VideoJuego juego) {
		this.colecjuego.add(juego);
	}
	
	public Boolean quitarJuego(String nombre) {
		Boolean eliminacion= false;
		for (VideoJuego videoJuego : this.colecjuego) {

			if (videoJuego.getNombreJuego().equals(nombre)) {
				colecjuego.remove(videoJuego);
				eliminacion=true;
			}
		}
		return eliminacion;
	}
	
	public void listaJuegos() {
		for (VideoJuego videoJuego : colecjuego) {
			System.out.println(videoJuego.InfoJuego());
		}
	}
	
	
}

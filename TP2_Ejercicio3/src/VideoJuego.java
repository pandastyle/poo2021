

public class VideoJuego {
	
	
 private String nombreJuego;
 private String anio;
 private String desarrollador;
 private Genero genero;
 
 
		public VideoJuego() {
			this("","","",Genero.NULO);
		}

		public VideoJuego(String nombreJuego, String anio, String desarrollador, Genero genero) {
			
			this.setNombreJuego(nombreJuego);
			this.setAnio(anio);
			this.setDesarrollador(desarrollador);
			this.setGenero(genero);
		}




		public String getNombreJuego() {
			return nombreJuego;
		}


		public void setNombreJuego(String nombreJuego) {
			this.nombreJuego = nombreJuego;
		}


		public String getAnio() {
			return anio;
		}


		public void setAnio(String anio) {
			this.anio = anio;
		}


		public String getDesarrollador() {
			return desarrollador;
		}


		public void setDesarrollador(String desarrollador) {
			this.desarrollador = desarrollador;
		}


		public Genero getGenero() {
			return genero;
		}


		public void setGenero(Genero genero) {
			this.genero = genero;
		}
		
		public String InfoJuego() {
			return "nombre: "+ this.getNombreJuego()+ " a�o: "+ this.getAnio()
			+ " desarrollador: "+ this.getDesarrollador()+ " genero: "+ this.getGenero();
		}
		
}





public class LimpiezaPilaExcepciones {

	public static void main(String[] args) {
		try {
			metodo1();
		}catch (Exception e) {
			System.err.printf("%s%n%n", e.getMessage());
			e.printStackTrace();
			
			StackTraceElement[] elementosRastreo= e.getStackTrace();
			
			System.out.printf("%n Rastreo de la pila de getStrackTrace:%n");
			System.out.printf("Clase\t\t\tArchivo\t\t\t\tLinea\tMetodo%n");
			
			for (StackTraceElement stackTraceElement : elementosRastreo) {
				System.out.printf("%s\t", stackTraceElement.getClassName());
				System.out.printf("%s\t", stackTraceElement.getFileName());
				System.out.printf("%s\t", stackTraceElement.getLineNumber());
				System.out.printf("%s%n", stackTraceElement.getMethodName());
			}
		}

	}
	
	public static void metodo1() throws Exception{
		metodo2();
	}
	public static void metodo2() throws Exception{
		metodo3();
	}
	public static void metodo3() throws Exception{
		throw new Exception("La excepcion se lanzo en metodo 3");
	}
	

}

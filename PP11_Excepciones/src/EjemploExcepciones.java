import java.util.InputMismatchException;
import java.util.Scanner;

public class EjemploExcepciones {

	public static void main(String[] args) {
		Scanner input= new Scanner(System.in);
		Boolean continuarCiclo= true;
		do {
			try {

		System.out.println("Introduzca un numerador entero");
		Integer numerador= input.nextInt();
		System.out.println("Introduzca un denominador entero");
		Integer denominador= input.nextInt();
		Integer resultado= cociente(numerador, denominador);
		System.out.printf("%nResultado: %d / %d = %d%n", numerador,denominador,resultado);
		continuarCiclo=false;
			}
			/*catch( InputMismatchException inputMismatchException) {
				System.err.printf("%n Excepcion: %s%n", inputMismatchException);//avisa que puso una letra
				input.nextLine();
				System.out.printf("Debe introducir enteros. Intente de nuevo. %n%n");
			}
			catch( ArithmeticException arithmeticException) {
				System.err.printf("%n Excepcion: %s%n", arithmeticException);//avisa que puso una letra
				input.nextLine();
				System.out.printf("Debe introducir enteros. Intente de nuevo. %n%n");
			}*/
			catch( InputMismatchException|ArithmeticException e) {//Multicatch
				System.err.printf("%n Excepcion: %s%n", e);//avisa que puso una letra
				input.nextLine();
				System.out.printf("Debe introducir enteros. Intente de nuevo. %n%n");
			}
			finally {//se ejecuta siempre, salte o no la excepcion
				System.out.println("Lo que esta en el bloque finaly siempre se ejecuta");
			}
		}while(continuarCiclo);

	}
	public static Integer cociente(Integer numerador,Integer denominador) {
		return numerador/denominador;
	}

}

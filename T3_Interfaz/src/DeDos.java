
public class DeDos implements Serie {

	int valor;
	int iniciar;
	int anterior;
	@Override
	public int getSiguiente() {
		valor +=2;
		return valor;
	}

	@Override
	public void reiniciar() {
		valor= iniciar;
		anterior= valor-2;
		
	}

	@Override
	public void setComenzar(int x) {
		iniciar=x;
		valor=x;
		anterior= x-2;
		
	}
	
	public int getAnterior() {
		return anterior;
	}

}

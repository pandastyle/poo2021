
public interface Serie {
	
	int VALOR= 0;
	int getSiguiente();
	void reiniciar();
	void setComenzar(int x);
}

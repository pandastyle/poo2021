import java.util.ArrayList;
import java.util.Iterator;

public class SerieDemo {

	public static void main(String[] args) {
		DeDos obDos= new DeDos();
		
		for (int i = 0; i < 5; i++) {
			System.out.println("Siguiente valor es: " + obDos.getSiguiente());
		}
		System.out.println("Reinicio \n");
		obDos.reiniciar();
		for (int i = 0; i < 5; i++) {
			System.out.println("Siguiente valor es: " + obDos.getSiguiente());
		}
		System.out.println("set de valor de inicio en 100\n");
		obDos.setComenzar(100);
		for (int i = 0; i < 5; i++) {
			System.out.println("Siguiente valor es: " + obDos.getSiguiente());
		}
		
		DeTres obTres= new DeTres();
		Serie obTemp;
		
		for (int i = 0; i < 5; i++) {
			obTemp= obDos;
			System.out.println("Siguiente valor es: " + obTemp.getSiguiente());
			obTemp= obTres;
			System.out.println("Siguiente valor es: " + obTemp.getSiguiente());
		}
		
		ArrayList<Serie> series= new ArrayList<Serie>();
		for (int i = 0; i < 3; i++) {
			series.add(new DeDos());
			series.add(new DeTres());
		}
		
		for (Serie s : series) {
		System.out.println("siguiente "+s.getSiguiente());
		}
		

	}

}

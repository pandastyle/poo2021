
public class Participante {
	private Integer numeroParticipante;
	private String nombre;
	
	
	public Participante(Integer numeroParticipante, String nombre) {
		
		this.numeroParticipante = numeroParticipante;
		this.nombre = nombre;
	}
	
	public Integer getNumeroParticipante() {
		return numeroParticipante;
	}
	public void setNumeroParticipante(Integer numeroParticipante) {
		this.numeroParticipante = numeroParticipante;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
}

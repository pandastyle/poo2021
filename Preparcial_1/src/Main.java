import java.util.ArrayList;
import java.util.Iterator;

public class Main {

	public static void main(String[] args) {
	
		
		ArrayList<Participante> jugadores= new ArrayList<Participante>();
		
	    for (int i = 1; i <= 16; i++) {
	    	Participante jugador  = new Participante(i,"jugador"+i);
	    	jugadores.add(jugador);
		}
	    
	    Grupos grupos= new Grupos("Competencia Grupos", jugadores);
	    grupos.armarFixture();
	    System.out.println("-----------LIGA-----------------");
	    Liga liga= new Liga("Competencia Liga", jugadores);
	    liga.armarFixture();
	    EliminacionDirecta eliminacion= new EliminacionDirecta("EliminacionDirecta", jugadores);
	    eliminacion.armarFixture();
	    
	   

	}

}

import java.util.ArrayList;

public abstract class Competicion {
	private String nombre_competicion;
	private ArrayList<Participante> participantes;
	
	public Competicion(String nombre_competicion, ArrayList<Participante> participantes) {
		
		this.nombre_competicion = nombre_competicion;
		this.participantes = participantes;
	}

	public String getNombre_competicion() {
		return nombre_competicion;
	}

	public void setNombre_competicion(String nombre_competicion) {
		this.nombre_competicion = nombre_competicion;
	}

	public ArrayList<Participante> getParticipantes() {
		return participantes;
	}

	public void setParticipantes(ArrayList<Participante> participantes) {
		this.participantes = participantes;
	}
	
	public abstract void armarFixture();
	
	
}

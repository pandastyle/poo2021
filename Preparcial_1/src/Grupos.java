import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Grupos extends Competicion {

	public Grupos(String nombre_competicion, ArrayList<Participante> participantes) {
		super(nombre_competicion, participantes);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void armarFixture() {
		Collections.shuffle(getParticipantes());
		List<Participante> grupo1= getParticipantes().subList(0, 4);
		List<Participante> grupo2=getParticipantes().subList(4, 8);
		List<Participante> grupo3= getParticipantes().subList(8, 12);
		List<Participante> grupo4= getParticipantes().subList(12, 16);
		
			System.out.println("GRUPO1:");
			for (Participante participante : grupo1) {
				System.out.println(participante.getNombre());
			}
			System.out.println("GRUPO2:");
			for (Participante participante : grupo2) {
				System.out.println(participante.getNombre());
			}
			System.out.println("GRUPO3:");
			for (Participante participante : grupo3) {
				System.out.println(participante.getNombre());
			}
			System.out.println("GRUPO4:");
			for (Participante participante : grupo4) {
				System.out.println(participante.getNombre());
			}
		
	
	
	}

}

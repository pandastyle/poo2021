import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;



public class EliminacionDirecta extends Competicion {

	public EliminacionDirecta(String nombre_competicion, ArrayList<Participante> participantes) {
		super(nombre_competicion, participantes);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void armarFixture() {
		Collections.shuffle(getParticipantes());
		System.out.println("Octavos de Final");
		ArrayList<Participante> cuartos= new ArrayList<Participante>();
		SecureRandom rnd= new SecureRandom();
		Integer j=7;
		for (int i =0 ; i <= 7; i++) {
			
			System.out.println( getParticipantes().get(i).getNombre() + "VS"+ getParticipantes().get(j).getNombre());
			
			
		
			Integer ganador=rnd.nextInt(2);
			
			if (ganador==0) {
				cuartos.add(getParticipantes().get(i));
			}else {
				cuartos.add(getParticipantes().get(j));
			}
			j++;
			
		}
		
		ArrayList<Participante> semifinal= new ArrayList<Participante>();
		System.out.println("CUARTOS DE FINAL");
		j=4;
		for (int i =0 ; i <= 3; i++) {
			
			System.out.println( cuartos.get(i).getNombre() + "VS"+ cuartos.get(j).getNombre());
			
			
			
			Integer ganador=rnd.nextInt(2);
			
			if (ganador==0) {
				semifinal.add(cuartos.get(i));
			}else {
				
				semifinal.add(cuartos.get(j));
			}
			++j;
		}
		
		System.out.println("SEMIFINAL");
		ArrayList<Participante> finalistas= new ArrayList<Participante>();
		j=2;
			for (int i =0 ; i <= 1; i++) {
			
			System.out.println( semifinal.get(i).getNombre() + "VS"+ semifinal.get(j).getNombre());
			
			
			
			Integer ganador=rnd.nextInt(2);
			
			
			if (ganador==0) {
				finalistas.add(semifinal.get(i));
			}else {
				finalistas.add(semifinal.get(j));
			}
			j++;
		}
		
			System.out.println("FINAL:");
			System.out.println( finalistas.get(0).getNombre() + "VS"+ finalistas.get(1).getNombre());

			
			Integer ganador=rnd.nextInt(2);

			if (ganador==0) {
				System.out.println("GANADOR:"+finalistas.get(0).getNombre() );
			}else {
				System.out.println("GANADOR:"+finalistas.get(1).getNombre() );
			}
		
		
	}

}

import java.util.LinkedList;

public class Servidor {
	private LinkedList<PaqueteDatos> colPaquete= new LinkedList<PaqueteDatos>();
	
	public synchronized PaqueteDatos obtenerDato() {
		while (colPaquete.isEmpty()) {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		PaqueteDatos paquete= colPaquete.removeFirst();
		
		if (paquete!=null) {
			System.out.println("OBTENER DATO: "+ paquete.toString());
		}
		notifyAll();
		return paquete;
		
	}
	
	public synchronized void agregarDato(PaqueteDatos unPaqueteDatos) {
		while (colPaquete.size()>=3) {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (unPaqueteDatos!=null) {
			System.out.println("AGREGA DATO: "+ unPaqueteDatos.toString());
		}
		this.colPaquete.add(unPaqueteDatos);
		notifyAll();
	}
}

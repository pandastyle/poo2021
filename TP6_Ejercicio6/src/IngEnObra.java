import java.util.Random;

public class IngEnObra extends IngenieroCivil implements agrimensor{
	private Random rand;
	
	public IngEnObra(String nombre, Servidor servidor) {
		super(nombre, servidor);
		rand= new Random();
	}
	//realiza 5 mediciones/tardan 3 seg
	public void run() {
		for (int i = 0; i < 5; i++) {
			try {
				this.enviarMedicion(this.medir());
			} catch (FallaInternetException e) {
				// TODO Auto-generated catch block
				System.out.println(e);
			}
			
		}
		this.getServidor().agregarDato(null);
	}
	@Override
	public PaqueteDatos medir() {
		PaqueteDatos paquete= new PaqueteDatos(rand.nextDouble()*91, rand.nextDouble()*181);
		return paquete;
	}
	@Override
	public void enviarMedicion(PaqueteDatos dato) throws FallaInternetException {
		try {
			Thread.sleep(3000);
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		if(new Random().nextInt(100)< 40) {
			throw new FallaInternetException("ERROR: ("+this.getNombre()+") No hay conexion a internet, no se envio paquete!");
		}
		this.getServidor().agregarDato(dato);
		
	}

}

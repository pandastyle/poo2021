package ejemploconexionbd;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Main {

	public static void main(String[] args) {
		
			UsuarioDao usuarioDao= new UsuarioDao();
			
			
			System.out.println(usuarioDao.get(1).toString());
			//usuarioDao.insert(new Usuario("Tomas","1111"));
			
			for (Usuario u1 : usuarioDao.getAll()) {
				System.out.println(u1.toString());
			}
			
			Usuario u= usuarioDao.get(1);
			u.setContrasenia("1234");
			u.setNombre("Matias");
			usuarioDao.update(u);
			
			for (Usuario u2 : usuarioDao.getAll()) {
				System.out.println(u2.toString());
			}
			
			System.out.println("BORRANDO ELEMENTO");
			//usuarioDao.remove(u);
			for (Usuario u2 : usuarioDao.getAll()) {
				System.out.println(u2.toString());
			}
			
		
		
	}

	
}

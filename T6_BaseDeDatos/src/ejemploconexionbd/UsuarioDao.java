package ejemploconexionbd;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class UsuarioDao {
	
	public ArrayList<Usuario> getAll(){
		ArrayList<Usuario> usuarios= new ArrayList<Usuario>();
		ResultSet rs= BasedeDatos.getInstance().getAll("SELECT* FROM public.usuario");//devuelve un resultset
		try {
			while (rs.next()) {//recorre la tabla
				Integer id = rs.getInt("id");
				String nombre = rs.getString("nombre");
				String contrasenia = rs.getString("contrasenia");
				usuarios.add(new Usuario(id,nombre,contrasenia));//se agrega al usuario al array
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return usuarios;
	}
	
	public Usuario get(Integer id) {
		Usuario usuario= null ;
		ResultSet rs= BasedeDatos.getInstance().getAll("SELECT* FROM public.usuario WHERE id="+id);//devuelve un resultset
		try {
			while (rs.next()) {//recorre la tabla
				Integer idrs = rs.getInt("id");
				String nombre = rs.getString("nombre");
				String contrasenia = rs.getString("contrasenia");
				usuario=new Usuario(idrs,nombre,contrasenia);//se agrega al usuario 
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return usuario;
	}
	
	public Integer insert(Usuario u) {
		return BasedeDatos.getInstance().alta("usuario", "nombre, contrasenia", "'"+u.getNombre()+"','"+u.getContrasenia()+"'");
	}
	
	public Boolean update(Usuario u) {
		return BasedeDatos.getInstance().update("usuario",u.getId(), "nombre = '"+u.getNombre()+"', contrasenia = '"+u.getContrasenia()+"'");
	}
	
	public Boolean remove(Usuario u) {
		return BasedeDatos.getInstance().remove("usuario", u.getId());
	}

}

package controlador;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import vista.VistaEspacio;

public class ControladorVE implements KeyListener {
	private VistaEspacio vista;
	
	public ControladorVE() {
		this.setVista(new VistaEspacio(this));
		this.getVista().setVisible(true);
	}

	public VistaEspacio getVista() {
		return vista;
	}

	public void setVista(VistaEspacio vista) {
		this.vista = vista;
	}

	@Override
	public void keyTyped(KeyEvent e) {
		Integer y= this.getVista().getLblNave().getY();
		Integer x= this.getVista().getLblNave().getX();
		Integer width=this.getVista().getLblNave().getWidth();
		Integer height=this.getVista().getLblNave().getHeight();
		switch (e.getKeyChar()) {
		case 'w': {
			//this.getVista().setBounds(x, y+1, width, height);
			this.getVista().getLblNewLabel().setVisible(true);
			break;
		}
		default:
			throw new IllegalArgumentException("Unexpected value: " + e);
		}
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	
	
}

package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorVE;

import javax.swing.JLabel;

public class VistaEspacio extends JFrame {

	private JPanel contentPane;
	private ControladorVE controlador;
	private JLabel lblNave;
	private JLabel lblNewLabel;
	/**
	 * Launch the application.
	 */
	
	public VistaEspacio(ControladorVE controlador) {
		this.setControlador(controlador);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 728, 441);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblNave = new JLabel("");
		
		lblNave.setBounds(252, 191, 165, 126);
		
		ImageIcon nave= new ImageIcon(new ImageIcon("img/nave.png").getImage()
				.getScaledInstance(lblNave.getWidth(), lblNave.getHeight(), Image.SCALE_DEFAULT));
		lblNave.setIcon(nave);
		contentPane.add(lblNave);
		
		lblNewLabel = new JLabel("Ejemplo");
		lblNewLabel.setBounds(66, 252, 46, 14);
		contentPane.add(lblNewLabel);
		lblNewLabel.setVisible(false);
		addKeyListener(getControlador());
	}

	public ControladorVE getControlador() {
		return controlador;
	}

	public void setControlador(ControladorVE controlador) {
		this.controlador = controlador;
	}

	public JLabel getLblNave() {
		return lblNave;
	}

	public void setLblNave(JLabel lblNave) {
		this.lblNave = lblNave;
	}

	public JLabel getLblNewLabel() {
		return lblNewLabel;
	}

	public void setLblNewLabel(JLabel lblNewLabel) {
		this.lblNewLabel = lblNewLabel;
	}
}

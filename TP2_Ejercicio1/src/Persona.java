import java.time.LocalDate;

public class Persona {
	private String nombre;
	private String genero;
	private String email;
	private LocalDate fecha_nacimiento;
	
	public Persona() {
		this("","","",LocalDate.of(1, 1, 1));
	}

	public Persona(String nombre, String genero, String email, LocalDate fecha_nacimiento) {
		
		this.nombre = nombre;
		this.genero = genero;
		this.email = email;
		this.fecha_nacimiento = fecha_nacimiento;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDate getFecha_nacimiento() {
		return fecha_nacimiento;
	}

	public void setFecha_nacimiento(LocalDate fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}
	
	
	
}

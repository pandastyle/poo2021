
public class Main {

	public static void main(String[] args) {
		EmpleadoPorComision emp1 = new EmpleadoPorComision("pepe", "Lui", "12123123", 300000.0, 0.1);
		EmpleadoBaseMasComision emp2= new EmpleadoBaseMasComision("juan", "velez", "321321321", 300000.0, 0.1, 50000.0);
		EmpleadoPorComision emp3 = new EmpleadoBaseMasComision("arturo", "perez", "346465", 300000.0, 0.1, 20000.0);
		
		System.out.println(emp1);
		System.out.println(emp2);
		System.out.println(emp3);

	}

}

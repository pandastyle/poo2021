
public class EmpleadoBaseMasComision extends EmpleadoPorComision{

	private Double salarioBase;
	
	
	public EmpleadoBaseMasComision(String nombre, String apellido, String dni, Double ventasBrutas,
			Double tarifaComision,Double salarioBase) {
		super(nombre, apellido, dni, ventasBrutas, tarifaComision);
		this.setSalarioBase(salarioBase);
		
	}
	
	public Double ingresos() {
		return getSalarioBase()+ super.ingresos();
	}
	
	public String toString() {
		return super.toString()+ " salario base:$"+ getSalarioBase();
	}


	public Double getSalarioBase() {
		return salarioBase;
	}


	public void setSalarioBase(Double salarioBase) {
		this.salarioBase = salarioBase;
	}

	
}

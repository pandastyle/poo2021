
public  class EmpleadoPorComision {
      private String nombre,apellido,dni;
      private Double ventasBrutas,tarifaComision;
      
	public EmpleadoPorComision(String nombre, String apellido, String dni, Double ventasBrutas, Double tarifaComision) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.dni = dni;
		this.ventasBrutas = ventasBrutas;
		this.tarifaComision = tarifaComision;
	}
	
	

	public Double getVentasBrutas() {
		return ventasBrutas;
	}

	public void setVentasBrutas(Double ventasBrutas) {
		this.ventasBrutas = ventasBrutas;
	}

	public Double getTarifaComision() {
		return tarifaComision;
	}

	public void setTarifaComision(Double tarifaComision) {
		this.tarifaComision = tarifaComision;
	}

	public String getNombre() {
		return nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public String getDni() {
		return dni;
	}



	@Override
	public String toString() {
		return "nombre: "+getNombre()+" apellido: "+getApellido()+" dni: "+getDni() + " Ventas brutas: "+getVentasBrutas()+
				" Tarifa comision: "+ getTarifaComision() + " Ingresos totales: $"+ingresos();
	}
	
	public Double ingresos() {
		return getVentasBrutas()*getTarifaComision();
	}
      
      
      
}

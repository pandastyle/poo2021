
public class Camara implements Grabar, GestionarArchivo {//extends que tiene hace referencia a herencia, en este caso seria implements dado que son interfaces

	private String direccion;
	
	public Camara() {
		
	}
	
	public Camara(String direccion) {
		this.direccion=direccion;//falta el this para hacer referencia al atributo de la clase
	}
	@Override
	public void crear(String direccion) {
		String dir= direccion == null? direccion: DIRECCCION_POR_DEFECTO;
		System.out.println("Creado archivo temporal en"+ dir);
	}

	@Override
	public void borrar() {
		System.out.println("se borro la grabacion");
		
	}

	@Override
	public void grabar() {
		crear(getDireccion());
		System.out.println("Grabando");
	}

	@Override
	public void detener() {
		System.out.println("finalizada la grabacion");
		
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	

}

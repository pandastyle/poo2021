import java.io.IOException;
import java.nio.file.Paths;
import java.util.Scanner;

public class Ejercicio10_LecturaCSV {
public static void main(String[] args) {
	try(Scanner consola= new Scanner(Paths.get("assets/persona.csv"))){
		//System.out.printf("%-12s%-12s%-10s%n","Nombre","Apellido","DNI");
		
		consola.useDelimiter("\\r\\n|\\n\\r");//hace separacion de datos por lineas
		String[] datos;
			while (consola.hasNext()) {
				datos = consola.next().split(";"); 
				//el .split sirve para cortar la cadena donde haya un ;
		System.out.println("Nombre:"+ datos[0]); 
		System.out.println("Apellido:"+datos[1] );
		System.out.println("DNI:"+Integer.valueOf(datos[2]));
		//Integer.valueOf(datos[2]) sirve para pasar de String a Integer
		 
		 
		}		
	} catch (IOException e) {//exception
		
		e.printStackTrace();
	}

}
}

import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Ejercicio10_CreacionCSV {

	public static void main(String[] args) {
		try(Formatter output = new Formatter("assets/persona.csv")){
			Scanner consola = new Scanner(System.in);
			System.out.printf("%s%n%sn?",
					"Ingrese Nombre, Apellido, DNI",
					"ingrese ctrl+z para finalizar.");
			
			while (consola.hasNext()) {//mientras existan elementos
				try {
				output.format("%s;%s;%d%n", consola.next(),consola.next(), consola.nextInt());	
			    }
				catch(NoSuchElementException elementException) {
				System.err.println("Valor Invalido. Ingrese otra vez");
				consola.nextLine();
			}//fin exception
				System.out.println("?");
				}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

}

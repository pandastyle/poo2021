
public class ModificadorContador implements Runnable {
	private Contador c;
	
	
	public ModificadorContador(Contador c) {
	
		this.c = c;
	}


	@Override
	public void run() {
		for (int i = 0; i < 5000; i++) {
			c.incrementar();
			System.out.println(Thread.currentThread().getName()+": "+ getC().getC());
		}

	}


	public Contador getC() {
		return c;
	}


	public void setC(Contador c) {
		this.c = c;
	}

}


public class MainContador {
public static void main(String[] args) {
	Contador contador= new Contador();
	Thread h1= new Thread(new ModificadorContador(contador));
	Thread h2= new Thread(new ModificadorContador(contador));
	Thread h3= new Thread(new ModificadorContador(contador));
	Thread h4= new Thread(new ModificadorContador(contador));
	h1.setName("Modificador 1");
	h2.setName("Modificador 2");
	h3.setName("Modificador 3");
	h4.setName("Modificador 4");
	
	h1.start();
	h2.start();
	h3.start();
	h4.start();
	
}
}

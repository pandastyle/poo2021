import java.security.SecureRandom;
import java.util.Iterator;
import java.util.Random;

public class EjemplosBasicos {

	public static void main(String[] args) {
		Math.pow(5, 2);//potencia de un numero
		Math.sqrt(900);//raiz 
		Math.abs(23.32);//valor absoluto de un numero
		Math.ceil(9.5); //el entero mas chico del parametro
		Math.floor(9.4);
		Math.cos(0.0);
		Math.sin(0);
		Math.tan(0);
		Math.log(Math.E);
		Math.max(2.3, 3.20);//maximo entre 2 valores
		
		Random rnd = new Random();
		rnd.nextInt();
		
		SecureRandom sran = new SecureRandom();
		System.out.println(sran.nextInt(2)+1);
		
		String cadena = "esto es una String";
		String cadena2 = new String("hola");
		
		for (int i = 0; i < cadena.length(); i++) {
			System.out.println(cadena.charAt(i));
		}
		
		for (char str : cadena.toCharArray()) {
			System.out.println(str);
		}
		
		System.out.println(cadena.substring(0,4));//muestra una parte de la cadena
		
		if (cadena2.equals("hola")) {
			System.out.println("con equals son iguales");
			
		}else {
			System.out.println("con equals no son iguales");
		}
		
		
		

	}

}

package Modelo;

public class Mascota {
 private Integer id, edad;
 private String nombre, especie;
 
public Mascota(Integer id, String nombre,String especie , Integer edad) {
	super();
	this.id = id;
	this.edad = edad;
	this.nombre = nombre;
	this.especie = especie;
}

public Integer getId() {
	return id;
}

public void setId(Integer id) {
	this.id = id;
}

public Integer getEdad() {
	return edad;
}

public void setEdad(Integer edad) {
	this.edad = edad;
}

public String getNombre() {
	return nombre;
}

public void setNombre(String nombre) {
	this.nombre = nombre;
}

public String getEspecie() {
	return especie;
}

public void setEspecie(String especie) {
	this.especie = especie;
}
 
 
}

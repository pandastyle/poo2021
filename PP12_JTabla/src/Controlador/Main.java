package Controlador;

import java.awt.EventQueue;

import Vista.VistaTablas;

public class Main {

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					new Controlador();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}

package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;import javax.swing.RowFilter;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import Modelo.Mascota;
import Vista.VistaTablas;

public class Controlador implements ListSelectionListener, ActionListener, KeyListener{
 private VistaTablas vista;

public Controlador() {
	super();
	this.setVista(new VistaTablas(this));
	Mascota mascota;
	for (int i = 0; i < 4; i++) {
		mascota = new Mascota((i+1),"mascota"+(i+1),"especie"+(i+1) ,new Random().nextInt(10)+1);
		Object [] row = {String.valueOf(mascota.getId()),mascota.getNombre(),mascota.getEspecie(),String.valueOf(mascota.getEdad())};
		this.getVista().getModeloTabla().addRow(row);
	}
	this.getVista().setVisible(true);
	
}

public VistaTablas getVista() {
	return vista;
}

public void setVista(VistaTablas vista) {
	this.vista = vista;
}

@Override
public void valueChanged(ListSelectionEvent e) {
	this.getVista().getBtnNewButton().setEnabled(true);//Habilita Boton
	
}

@Override
public void actionPerformed(ActionEvent e) {
	Object [] vacio = {0,"","",null};
	if (e.getSource()== getVista().getBtnNewButton()) {// tambien puede ser e.getSource().equals(getVista().getBtnNewButton()))
		//this.getVista().getTable().getSelectedRow();//fila que seleccione
		//System.out.println(this.getVista().getTable().getValueAt(this.getVista().getTable().getSelectedRow(), 1));//obtiene dato de la tabla
		//this.getVista().getModeloTabla().setValueAt("pepe", this.getVista().getTable().getSelectedRow(), 1);//modifica valores de la tabla

		//this.getVista().getModeloTabla().removeRow(this.getVista().getTable().getSelectedRow());//remueve fila
		
	}
	
	if (e.getSource().equals(getVista().getBtnNewButton_1())) {
		this.getVista().getModeloTabla().addRow(vacio);
	}
	
}

public void search() {//busca elemento de la tabla
	TableRowSorter<DefaultTableModel> tr = new TableRowSorter<DefaultTableModel>(getVista().getModeloTabla());
	this.getVista().getTable().setRowSorter(tr);
	tr.setRowFilter(RowFilter.regexFilter(this.getVista().getTextField().getText()));
	
}

@Override
public void keyTyped(KeyEvent e) {
	// TODO Auto-generated method stub
	
}

@Override
public void keyPressed(KeyEvent e) {
	this.search();
	
}

@Override
public void keyReleased(KeyEvent e) {
	this.search();
	
}
 
}

package Vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import Controlador.Controlador;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.JTextField;

public class VistaTablas extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JButton btnNewButton;
	private DefaultTableModel modeloTabla;
	private Controlador controlador;
	private JButton btnNewButton_1;
	private JLabel lblNewLabel;
	private JTextField textField;

	
	public VistaTablas(Controlador controlador) {
		setControlador(controlador);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 479, 304);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblMascotas = new JLabel("Mascotas");
		lblMascotas.setBounds(129, 9, 140, 14);
		contentPane.add(lblMascotas);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 57, 327, 193);
		contentPane.add(panel);
		panel.setLayout(null);
		
		String header[] = {"ID","Nombre","Especie","Edad"};
		modeloTabla= new DefaultTableModel(null,header);
				
		table = new JTable(modeloTabla);//primero tabla
		table.setBounds(0, 0, 327, 193);
		table.setVisible(true);
		table.setDefaultEditor(Object.class, null);//no deja editar la tabla
		table.getColumnModel().getColumn(0).setMaxWidth(0);
		table.getColumnModel().getColumn(0).setMinWidth(0);
		table.getColumnModel().getColumn(0).setPreferredWidth(0);
		
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION); //solo deja seleccionar una fila
		table.getSelectionModel().addListSelectionListener(this.getControlador());
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 0, 327, 193);
		scrollPane.setViewportView(table);// se pone tabla en el scroll pane
		panel.add(scrollPane);
		
		

		
		btnNewButton = new JButton("Ejemplo");
		btnNewButton.setEnabled(false);
		btnNewButton.setBounds(359, 57, 89, 23);
		btnNewButton.addActionListener(getControlador());
		contentPane.add(btnNewButton);
		
		btnNewButton_1 = new JButton("Agregar");
		btnNewButton_1.setBounds(359, 84, 89, 23);
		btnNewButton_1.addActionListener(getControlador());
		contentPane.add(btnNewButton_1);
		
		lblNewLabel = new JLabel("Buscar:");
		lblNewLabel.setBounds(10, 34, 46, 14);
		contentPane.add(lblNewLabel);
		
		textField = new JTextField();
		textField.setBounds(50, 34, 287, 20);
		textField.addKeyListener(getControlador());
		contentPane.add(textField);
		textField.setColumns(10);
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public JButton getBtnNewButton() {
		return btnNewButton;
	}

	public void setBtnNewButton(JButton btnNewButton) {
		this.btnNewButton = btnNewButton;
	}

	public DefaultTableModel getModeloTabla() {
		return modeloTabla;
	}

	public void setModeloTabla(DefaultTableModel modeloTabla) {
		this.modeloTabla = modeloTabla;
	}

	public Controlador getControlador() {
		return controlador;
	}

	public void setControlador(Controlador controlador) {
		this.controlador = controlador;
	}

	public JButton getBtnNewButton_1() {
		return btnNewButton_1;
	}

	public void setBtnNewButton_1(JButton btnNewButton_1) {
		this.btnNewButton_1 = btnNewButton_1;
	}

	public JTextField getTextField() {
		return textField;
	}

	public void setTextField(JTextField textField) {
		this.textField = textField;
	}
	
}

package vista;
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorVP;

import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Color;

public class VistaPrincipal extends JFrame {

	private JPanel contentPane;
	private JCheckBox chLentes;
	private JCheckBox chMenton;
	private JCheckBox chPelo;
	private JCheckBox chDientes;
	private JLabel lblimagen;
	private ControladorVP controlador;

	/**
	 * Launch the application.
	 */
	
	/**
	 * Create the frame.
	 */
	public VistaPrincipal(ControladorVP controlador) {
		this.setControlador(controlador);
		setTitle("TP4-Imagenes");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 333, 230);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		chLentes = new JCheckBox("Lentes");
		chLentes.addChangeListener(controlador);
		chLentes.setBounds(19, 29, 97, 23);
		contentPane.add(chLentes);
		
		chMenton = new JCheckBox("Menton");
		chMenton.addChangeListener(controlador);
		chMenton.setBounds(19, 62, 97, 23);
		contentPane.add(chMenton);
		
		chPelo = new JCheckBox("Pelo");
		chPelo.addChangeListener(controlador);
		chPelo.setBounds(19, 96, 97, 23);
		contentPane.add(chPelo);
		
		chDientes = new JCheckBox("Dientes");
		chDientes.addChangeListener(controlador);
		chDientes.setBounds(19, 131, 97, 23);
		contentPane.add(chDientes);
		
		lblimagen = new JLabel("");
		lblimagen.setForeground(Color.RED);
		lblimagen.setHorizontalAlignment(SwingConstants.CENTER);
		lblimagen.setBounds(171, 29, 136, 121);
		ImageIcon defecto= new ImageIcon(new ImageIcon("img/0000.gif").getImage().
				getScaledInstance(lblimagen.getWidth(), lblimagen.getHeight(), Image.SCALE_DEFAULT));
		lblimagen.setIcon(defecto);
		contentPane.add(lblimagen);
		this.setLocationRelativeTo(null);
	}

	public JCheckBox getChLentes() {
		return chLentes;
	}

	public void setChLentes(JCheckBox chLentes) {
		this.chLentes = chLentes;
	}

	public JCheckBox getChMenton() {
		return chMenton;
	}

	public void setChMenton(JCheckBox chMenton) {
		this.chMenton = chMenton;
	}

	public JCheckBox getChPelo() {
		return chPelo;
	}

	public void setChPelo(JCheckBox chPelo) {
		this.chPelo = chPelo;
	}

	public JCheckBox getChDientes() {
		return chDientes;
	}

	public void setChDientes(JCheckBox chDientes) {
		this.chDientes = chDientes;
	}

	public JLabel getLblimagen() {
		return lblimagen;
	}

	public void setLblimagen(JLabel lblimagen) {
		this.lblimagen = lblimagen;
	}

	public ControladorVP getControlador() {
		return controlador;
	}

	public void setControlador(ControladorVP controlador) {
		this.controlador = controlador;
	}
}

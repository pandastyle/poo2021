package controlador;

import java.awt.Image;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ImageIcon;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import vista.VistaPrincipal;

public class ControladorVP implements ChangeListener {
	 private VistaPrincipal vista;
	
	public ControladorVP() {
		this.setVista(new VistaPrincipal(this));
		this.getVista().setVisible(true);
	}



	public VistaPrincipal getVista() {
		return vista;
	}

	public void setVista(VistaPrincipal vista) {
		this.vista = vista;
	}



	@Override
	public void stateChanged(ChangeEvent e) {
		String ruta="img/0000.gif";
		if(getVista().getChLentes().isSelected()) {
			ruta="img/1000.gif";
		}
		
		if(getVista().getChMenton().isSelected()) {
			ruta="img/0100.gif";
		}
		
		if(getVista().getChPelo().isSelected()) {
			ruta="img/0010.gif";
		}
		if(getVista().getChDientes().isSelected()) {
			ruta="img/0001.gif";
		}
		
		//pelo y dientes
		if(getVista().getChPelo().isSelected() && getVista().getChDientes().isSelected()) {
			ruta="img/0011.gif";
		}
		//pelo y menton
		if(getVista().getChPelo().isSelected() && getVista().getChMenton().isSelected()) {
			ruta="img/0110.gif";
		}
		//pelo y lentes
		if(getVista().getChPelo().isSelected() && getVista().getChLentes().isSelected()) {
			ruta="img/1010.gif";
		}
		
		//menton y dientes
		if(getVista().getChMenton().isSelected() && getVista().getChDientes().isSelected()) {
			ruta="img/0101.gif";
		}
		//menton y lentes
		if(getVista().getChMenton().isSelected() && getVista().getChLentes().isSelected()) {
			ruta="img/1100.gif";
		}
		//lentes y dientes
		if(getVista().getChLentes().isSelected() && getVista().getChDientes().isSelected()) {
			ruta="img/1001.gif";
		}
		//lentes, menton y pelo
		if(getVista().getChPelo().isSelected() && getVista().getChLentes().isSelected() && getVista().getChMenton().isSelected()) {
			ruta="img/1110.gif";
		}
		//lentes, menton y dientes
		if(getVista().getChLentes().isSelected() && getVista().getChMenton().isSelected()&& getVista().getChDientes().isSelected()) {
			ruta="img/1101.gif";
		}
		//lentes pelo y dientes
		if(getVista().getChLentes().isSelected() && getVista().getChDientes().isSelected() && getVista().getChPelo().isSelected()) {
			ruta="img/1011.gif";
		}
		//dientes pelo y menton
		if(getVista().getChPelo().isSelected() && getVista().getChDientes().isSelected()&&getVista().getChMenton().isSelected()) {
			ruta="img/0111.gif";
		}
		
		//todo
		if(getVista().getChPelo().isSelected() && getVista().getChDientes().isSelected()&&getVista().getChMenton().isSelected()&&getVista().getChLentes().isSelected()) {
			ruta="img/1111.gif";
		}
		
		
		ImageIcon seleccion= new ImageIcon(new ImageIcon(ruta).getImage().
				getScaledInstance(getVista().getLblimagen().getWidth(), getVista().getLblimagen().getHeight(), Image.SCALE_DEFAULT));
		
		getVista().getLblimagen().setIcon(seleccion);
		
		
	}
}

import javax.swing.JOptionPane;

public class EjemploJOptionPane {

	public static void main(String[] args) {
		//JOptionPane.showMessageDialog(null, "Hola!!");
		
		String nombre= JOptionPane.showInputDialog("Como te llamas?");
		String mensaje= String.format("Bienvenido, %s, a la clase de POO", nombre);
		JOptionPane.showMessageDialog(null, mensaje);

	}

}

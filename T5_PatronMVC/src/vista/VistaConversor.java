package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorMoneda;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;

public class VistaConversor extends JFrame {

	private JPanel contentPane;
	private JTextField txtValor;
	private JTextField txtResultado;
	private JButton btnDolares;
	private JButton btnPesos;
	private ControladorMoneda cm; //se agrega el controlador que lo maneja para que lo conozca
	
	
	/*la vista recibe el controlador que lo maneja*/
	public VistaConversor(ControladorMoneda cont) {
		setTitle("Conversor de Monedas");
		this.setCm(cont);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
	    
		txtValor = new JTextField();
		txtValor.setBounds(155, 69, 86, 20);
		contentPane.add(txtValor);
		txtValor.setColumns(10);
		
		txtResultado = new JTextField();
		txtResultado.setEditable(false);
		txtResultado.setBounds(155, 112, 86, 20);
		contentPane.add(txtResultado);
		txtResultado.setColumns(10);
		
		JLabel lblValor = new JLabel("Valor");
		lblValor.setBounds(99, 72, 46, 14);
		contentPane.add(lblValor);
		
		JLabel lblResultado = new JLabel("Resultado");
		lblResultado.setBounds(87, 115, 58, 14);
		contentPane.add(lblResultado);
		
		btnDolares = new JButton("d\u00F3lares");
		btnDolares.setBounds(76, 168, 89, 23);
		contentPane.add(btnDolares);
		getBtnDolares().addActionListener(this.getCm());//se le indica a cada componente que genera un evento cual es el controlador
		
		btnPesos = new JButton("pesos");
		btnPesos.setBounds(190, 168, 89, 23);
		getBtnPesos().addActionListener(this.getCm());
		contentPane.add(btnPesos);
		
	}



	public JTextField getTxtValor() {
		return txtValor;
	}



	public void setTxtValor(JTextField txtValor) {
		this.txtValor = txtValor;
	}



	public JTextField getTxtResultado() {
		return txtResultado;
	}



	public void setTxtResultado(JTextField txtResultado) {
		this.txtResultado = txtResultado;
	}



	public JButton getBtnDolares() {
		return btnDolares;
	}



	public void setBtnDolares(JButton btnDolares) {
		this.btnDolares = btnDolares;
	}



	public JButton getBtnPesos() {
		return btnPesos;
	}



	public void setBtnPesos(JButton btnPesos) {
		this.btnPesos = btnPesos;
	}



	public ControladorMoneda getCm() {
		return cm;
	}



	public void setCm(ControladorMoneda cm) {
		this.cm = cm;
	}
}

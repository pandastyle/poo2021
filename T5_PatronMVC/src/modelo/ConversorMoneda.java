package modelo;

public class ConversorMoneda {
	
	private Double valorDolar;
	
	public ConversorMoneda(Double valorDolar)
	{
		this.setValorDolar(valorDolar);
	}
	
	public Double convertirPesosADolares(Double pesos)
	{
		return pesos/this.getValorDolar();
	}
	
	public Double convertirDolaresAPesos(Double dolares)
	{
		return dolares*this.getValorDolar();
	}

	public Double getValorDolar() {
		return valorDolar;
	}

	public void setValorDolar(Double valorDolar) {
		this.valorDolar = valorDolar;
	}

}

package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

import modelo.ConversorMoneda;
import vista.VistaConversor;

public class ControladorMoneda implements ActionListener {

	private ConversorMoneda cm;
	private VistaConversor vc;
	
	public ControladorMoneda(Double valorDolar)
	{
		this.setVc(new VistaConversor(this));//aca se instancia la vista y se pasa el controlador que lo maneja para que lo conozca
	    this.setCm(new ConversorMoneda(valorDolar)); 
	}
	
	@Override
	public void actionPerformed(ActionEvent evento) {
		// TODO Auto-generated method stub

		Double valorAConvertir = Double.parseDouble(this.getVc().getTxtValor().getText());
		Double resultado=0.0;
		if(evento.getSource()==this.getVc().getBtnDolares())
		{
			
			resultado = this.getCm().convertirPesosADolares(valorAConvertir);
			
		}
		else if(evento.getSource()==this.getVc().getBtnPesos())
		{
			
			resultado = this.getCm().convertirDolaresAPesos(valorAConvertir);
			
		}
		DecimalFormat df = new DecimalFormat("#.00"); //se usa la clase DecimalFormat para la precisión de 2 decimales en el resultado
		
		this.getVc().getTxtResultado().setText(df.format(resultado));
	}

	public ConversorMoneda getCm() {
		return cm;
	}

	public void setCm(ConversorMoneda cm) {
		this.cm = cm;
	}

	public VistaConversor getVc() {
		return vc;
	}

	public void setVc(VistaConversor vc) {
		this.vc = vc;
	}

	

}

package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

import javax.swing.JFileChooser;

//import org.omg.CORBA.ARG_OUT;

import Modelo.Reproductor;
import Vista.Vista;
import javazoom.jlgui.basicplayer.BasicController;
import javazoom.jlgui.basicplayer.BasicPlayerEvent;
import javazoom.jlgui.basicplayer.BasicPlayerException;
import javazoom.jlgui.basicplayer.BasicPlayerListener;

public class Controlador implements ActionListener{

	private Reproductor repro;
	private Vista vista;
	
	public Controlador() {
		super();
		this.setRepro(new Reproductor());
		this.setVista(new Vista(this));
		this.getVista().setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(this.getVista().getBtnAbrir())) {
			
			JFileChooser fc = new JFileChooser();
			int sel = fc.showOpenDialog(getVista());
			
			if(sel == JFileChooser.APPROVE_OPTION) {
				try {
					this.getVista().getTextField().setText(fc.getSelectedFile().getAbsolutePath());;
					this.getRepro().abrir(fc.getSelectedFile().getAbsolutePath());
				} catch (BasicPlayerException e1) {
					e1.printStackTrace();
				}
			}
			
		}else if(e.getSource().equals(this.getVista().getBtnPause())) {
			try {
				this.getRepro().pausa();
			} catch (BasicPlayerException e1) {
				e1.printStackTrace();
			}
		}else if(e.getSource().equals(this.getVista().getBtnPlay()))
			try {
				this.getRepro().play();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		else if(e.getSource().equals(this.getVista().getBtnStop())) {
			try {
				this.getRepro().stop();
			} catch (BasicPlayerException e1) {
				e1.printStackTrace();
			}
		}else if(e.getSource().equals(this.getVista().getBtnContinuar())) {
			try {
				this.getRepro().resume();
			} catch (BasicPlayerException e1) {
				e1.printStackTrace();
			}
		}
		
	}

	public Vista getVista() {
		return vista;
	}

	public void setVista(Vista vista) {
		this.vista = vista;
	}

	public Reproductor getRepro() {
		return repro;
	}

	public void setRepro(Reproductor repro) {
		this.repro = repro;
	}
	

}

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Anemometro implements Sensor {

	@Override
	public Double sensar() {
		// TODO Auto-generated method stub
		//return new Random().nextDouble()*61;//de 0 a 60km
		return ThreadLocalRandom.current().nextDouble(60.1);
	}

}

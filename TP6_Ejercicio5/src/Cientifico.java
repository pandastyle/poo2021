import java.util.LinkedList;

public class Cientifico extends Thread {
	private Servidor servidor;
	public LinkedList<PaqueteDato> colaPaquetes= new LinkedList<PaqueteDato>();
	
	public Cientifico(Servidor servidor) {
		
		this.servidor = servidor;
	}
	
	private void promediar(String nombreBoya,String tipoDato) {
		Double suma=0.0;
		Integer total=0;
		
		for (PaqueteDato paqueteDato : colaPaquetes) {
			if (paqueteDato.getNombreBoya().equals(nombreBoya)) {
				if (tipoDato.equals("temperatura")) {
					suma= suma + paqueteDato.getTemperatura();
					
				}else {
					suma= suma + paqueteDato.getVelocidadViento();
				}
				total++;
			}
		}
		System.out.println("El promedio de "+tipoDato+" para la boya "+ nombreBoya+ " es: " + (suma/total));
		
	}
	
	public void run() {
		Integer contador=0;
		while (contador<2) {
		PaqueteDato paquete=this.getServidor().consultar();
			if (paquete==null) {
				contador++;
			}else {
				this.getColaPaquetes().add(paquete);
			}
		}
		System.out.println("\nINFORME PROMEDIOS");
		this.promediar("CIDMAR-1", "temperatura");
		this.promediar("CIDMAR-1", "velocidad de Viento");
		this.promediar("CIDMAR-2", "temperatura");
		this.promediar("CIDMAR-2", "velocidad de Viento");
		
		
	}

	public Servidor getServidor() {
		return servidor;
	}

	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}

	public LinkedList<PaqueteDato> getColaPaquetes() {
		return colaPaquetes;
	}

	public void setColaPaquetes(LinkedList<PaqueteDato> colaPaquetes) {
		this.colaPaquetes = colaPaquetes;
	}
	
	
	
	
}

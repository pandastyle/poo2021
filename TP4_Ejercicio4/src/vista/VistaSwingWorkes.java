package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorSwingWorkes;

import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JButton;

public class VistaSwingWorkes extends JFrame {

	private JPanel contentPane;
	private JButton btnComenzar;
	private ControladorSwingWorkes controlador;
	private JProgressBar pBar3;
	private JProgressBar pBar2;
	private JProgressBar pBar1;
	
	public VistaSwingWorkes(ControladorSwingWorkes controlador) {
		this.setControlador(controlador);
		setTitle("Swing Workers");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 427, 266);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblServicio1 = new JLabel("Servicio 1");
		lblServicio1.setBounds(20, 56, 79, 14);
		contentPane.add(lblServicio1);
		
		JLabel lblServicio2 = new JLabel("Servicio 2");
		lblServicio2.setBounds(20, 90, 79, 14);
		contentPane.add(lblServicio2);
		
		JLabel lblServicio3 = new JLabel("Servicio 3");
		lblServicio3.setBounds(20, 134, 79, 14);
		contentPane.add(lblServicio3);
		
		pBar1 = new JProgressBar();
		pBar1.setBounds(83, 56, 240, 23);
		contentPane.add(pBar1);
		
		pBar2 = new JProgressBar();
		pBar2.setBounds(83, 90, 240, 23);
		contentPane.add(pBar2);
		
		pBar3 = new JProgressBar();
		pBar3.setBounds(83, 125, 240, 23);
		contentPane.add(pBar3);
		
		btnComenzar = new JButton("Comenzar");
		btnComenzar.addActionListener(controlador);
		btnComenzar.setBounds(148, 173, 100, 23);
		contentPane.add(btnComenzar);
	}

	public ControladorSwingWorkes getControlador() {
		return controlador;
	}

	public void setControlador(ControladorSwingWorkes controlador) {
		this.controlador = controlador;
	}

	public JButton getBtnComenzar() {
		return btnComenzar;
	}

	public JProgressBar getpBar3() {
		return pBar3;
	}

	public JProgressBar getpBar2() {
		return pBar2;
	}

	public JProgressBar getpBar1() {
		return pBar1;
	}
}

package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;

import vista.VistaSwingWorkes;

public class ControladorSwingWorkes implements ActionListener {
	private VistaSwingWorkes vista;

	
	public ControladorSwingWorkes() {
		this.setVista(new VistaSwingWorkes(this));
		this.getVista().setVisible(true);
	}

	public VistaSwingWorkes getVista() {
		return vista;
	}

	public void setVista(VistaSwingWorkes vista) {
		this.vista = vista;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(getVista().getBtnComenzar())) {
			
			for (int i =0;i<=100;++i) {
				try {
					Thread.sleep(100);
					this.getVista().getpBar1().setValue(i);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			
				
			}
		}
		
	}
	
	
}

import java.util.Iterator;

public class AS_ResumenBasico {

	public static void main(String[] args) {
		//comentario simple
		/**Java Doc **/
		
		//--------------------TIPOS PRIMITIVOS---------------------------------
		System.out.println("----------TIPOS PRIMITIVOS------------");
		int numEntero= 8;
		double numDecimal= 2.8;
		boolean rta= true;
		char caracter= 'A';
		
		System.out.println("Numero Entero: " + numEntero);
		System.out.println("Numero Flotante: " + numDecimal);
		System.out.println("Booleano: " + rta);
		System.out.println("Caracter: " + caracter);
		//casting
		System.out.println("***CASTING***");
		int casteo= (int)2.5;
		System.out.println("Numero "+ casteo);
		System.out.println();
		//--------------------OPERADORES LOGICOS---------------------------------
		System.out.println("----------OPERADORES LOGICOS------------");
		if (numEntero == numDecimal) {
			System.out.println("Los numeros son iguales");
		}else if (numEntero > numDecimal) {
			System.out.println("El Numero entero es Mayor a Numero Decimal");
		}else {
			System.out.println("El Numero entero es Menor a Numero Decimal");
		}
		/*
		 * igualdad ==
		 * desigual !=
		 * mayor > menor <
		 * mayor igual >= menor igual <=
		 * */
		
		if (numEntero != numDecimal && numEntero > 7) {
			System.out.println("El numero entero es distinto y mayor que 7");
		}
		/*
		 * &: y 
		 * |: o
		 * &&: y, pero no evalua la segunda parte si la primera es falsa
		 * ||: o, pero no evalua la segunda parte si la primera es verdadera
		 */
		//  IF CORTO
		System.out.println(numEntero>numDecimal? "entero > flotante":"entero <= flotante");
		
		int resultado= numEntero !=0? 100/numEntero : 0;
		System.out.println();
		//--------------------SWITCH / CASE---------------------------------
		System.out.println("----------SWITCH / CASE------------");
		switch (caracter) {
		case 'A': {
			System.out.println("el caracter seleccionado es el A");
			break;
		}
		
		case 'B': {
			System.out.println("el caracter seleccionado es el B");
			break;
		}
		default:
			throw new IllegalArgumentException("Unexpected value: " + caracter);
		}
		System.out.println();
		//--------------------BUCLES - ARRAYS---------------------------------
		System.out.println("----------BUCLES - ARRAYS------------");
		int [] numerosEnteros= {1,3,5,2,6,8,7};
		int i = 0 ;// la posicion del indice siempre comienza en 0
		
		//*******WHILE**********
		System.out.println("*******WHILE**********");
		while (i<numerosEnteros.length) {
			System.out.println("Numero en while "+numerosEnteros[i]);
			i++;
		}
		
		//*******DO WHILE**********
		System.out.println("*******DO WHILE**********");
		int j = 6;
		do {
			numerosEnteros[j]= j;
			System.out.println("Numero en do while "+ numerosEnteros[j]);
			j--;
		} while (j>=0);
		
		//*******FOR**********
		System.out.println("*******FOR**********");
		for (int k = 0; k < numerosEnteros.length; k++) {
			System.out.println("Numero en for "+ numerosEnteros[k]);
		}
		
		//*******FOR EACH**********
		System.out.println("*******FOR EACH**********");
		for (int f : numerosEnteros) {
			System.out.println("Numero en foreach "+ numerosEnteros[f]);
		}
		
		//Operadores de Asignacion Compuesto
		i+=7;//suma 7
		i-=4;//resta 4
		i*=5;//multiplica por 5
		i/=3;//divide por 3
		i%=9;//resto
		
		++i;//incrementa en uno y usa el valor
		i++;// usa el valor y lo incrementa

	}

}

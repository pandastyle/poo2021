package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorVP;

import javax.swing.JButton;
import javax.swing.JSpinner;
import javax.swing.JComboBox;
import javax.swing.JLabel;

public class VistaPrincipal extends JFrame {

	private JPanel contentPane;
	private JButton btnMostrar;
	private JComboBox BoxIdioma;
	private JComboBox BoxMensaje;
	private ControladorVP controlador;

	/**
	 * Launch the application.
	 */
	

	/**
	 * Create the frame.
	 */
	public VistaPrincipal(ControladorVP controlador) {
		this.setControlador(controlador);
		setTitle("Mensajes");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 462, 224);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		btnMostrar = new JButton("Mostrar");
		btnMostrar.setBounds(177, 134, 89, 23);
		btnMostrar.addActionListener(getControlador());//se le setea el controlador que realiza la accion
		contentPane.add(btnMostrar);
		
		String[] idiomas= {"---","Portuges","Frances","Ingles"};
		BoxIdioma = new JComboBox(idiomas);// se agregan las opciones al dropdown
		BoxIdioma.setSelectedItem("");//Muestra por defecto
		BoxIdioma.setBounds(84, 34, 293, 22);
		contentPane.add(BoxIdioma);
		
		JLabel lblIdioma = new JLabel("Seleccione Idioma:");
		lblIdioma.setBounds(85, 11, 127, 14);
		contentPane.add(lblIdioma);
		
		String[] mensajes= {"---","saludar","despedirse","perdon","pedir cafe","�Cuanto Cuesta?","�Donde Queda?"};
		BoxMensaje = new JComboBox(mensajes);
		BoxMensaje.setSelectedItem("---");
		/*String mensajeElegido=(String)BoxMensaje.getSelectedItem(); muestra el valor seleccionado
		System.out.println(mensajeElegido+" A");*/
		
		BoxMensaje.setBounds(84, 89, 293, 22);
		contentPane.add(BoxMensaje);
		
		JLabel lblmensaje = new JLabel("Seleccione Mensaje:");
		lblmensaje.setBounds(85, 66, 127, 14);
		contentPane.add(lblmensaje);
		this.setLocationRelativeTo(null);
	}

	public ControladorVP getControlador() {
		return controlador;
	}

	public void setControlador(ControladorVP controlador) {
		this.controlador = controlador;
	}

	public JButton getBtnMostrar() {
		return btnMostrar;
	}

	public JComboBox getBoxIdioma() {
		return BoxIdioma;
	}

	public JComboBox getBoxMensaje() {
		return BoxMensaje;
	}
}

package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import modelo.Frances;
import modelo.Idioma;
import modelo.Ingles;
import modelo.Portuges;
import vista.VistaPrincipal;

public class ControladorVP implements ActionListener {
 private VistaPrincipal vista;
 private Idioma idiomas;

public ControladorVP() {
	super();
	this.vista = new VistaPrincipal(this);
	this.vista.setVisible(true);
}

public VistaPrincipal getVista() {
	return vista;
}

public void setVista(VistaPrincipal vista) {
	this.vista = vista;
}

@Override
public void actionPerformed(ActionEvent e) {
	if(e.getSource().equals(getVista().getBtnMostrar())) {
		String idiomaElegido=(String)getVista().getBoxIdioma().getSelectedItem();//asigna el valor del box
		//System.out.println(idiomaElegido);
		
		//se asigna a idiomas el componente correspondiente al idioma
		switch (idiomaElegido) {//se aplica polimorfismo
		case "Portuges": {
			idiomas= new Portuges();
			break;
		}
		
		case "Ingles": {
			idiomas= new Ingles();	
					break;
				}
		case "Frances": {
			idiomas= new Frances();
			break;
		}
		default:
			throw new IllegalArgumentException("No Es correcto " + idiomaElegido);
		}
		
		//asignamos el mensaje seleccionado
		String mensajeElegido= (String)getVista().getBoxMensaje().getSelectedItem();//asigna el valor del box
		//System.out.println(mensajeElegido);
		
		switch (mensajeElegido) {
		case "saludar": {
			JOptionPane.showMessageDialog(null, idiomas.saludar());
			break ;
		}
		
		case "despedirse": {
			JOptionPane.showMessageDialog(null, idiomas.despedirse());
					break ;
				}
		case "perdon": {
			JOptionPane.showMessageDialog(null, idiomas.perdon());
			break ;
		}
		case "pedir cafe": {
			JOptionPane.showMessageDialog(null, idiomas.pedirCafe());
			break ;
		}
		case "�Cuanto Cuesta?": {
			JOptionPane.showMessageDialog(null, idiomas.cuantoCuesta());
			break ;
		}
		case "�Donde Queda?": {
			JOptionPane.showMessageDialog(null, idiomas.dondeQueda());
			break ;
		}
		default:
			throw new IllegalArgumentException("Unexpected value: " + mensajeElegido);
		}
		
	}
	
}
 
 
}

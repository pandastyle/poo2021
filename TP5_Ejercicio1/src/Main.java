import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner input= new Scanner(System.in);
		Integer resultado;
		try {

			System.out.println("Introduzca un numerador entero");
			Integer numerador= input.nextInt();
			System.out.println("Introduzca un denominador entero");
			Integer denominador= input.nextInt();
			resultado= numerador/denominador;
			System.out.printf("%nla division es: %d / %d = %d%n", numerador,denominador,resultado);
			}
			catch(ArithmeticException e) {
				System.out.println("no se puede dividir por cero! se asignara 0 al resultado");
				resultado=0;
				System.out.println("la division es:" + resultado);
				
			}
		
		

	}

}

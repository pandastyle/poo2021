
public class EstadoConexionDesconectado extends EstadoConexionBD {

	@Override
	protected String conectarBD(BaseDeDatos bd) {
	    bd.setEstado(new EstadoConexionConectado());
		return "Conectando a la BD";
	}

	@Override
	protected String desconectarBD(BaseDeDatos bd) {
		// TODO Auto-generated method stub
		return "Usted ya esta desconectado";
	}

}

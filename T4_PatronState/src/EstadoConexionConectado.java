
public class EstadoConexionConectado extends EstadoConexionBD{

	@Override
	protected String conectarBD(BaseDeDatos bd) {
		
		return "Usted ya esta conectado";
	}

	@Override
	protected String desconectarBD(BaseDeDatos bd) {
		bd.setEstado(new EstadoConexionDesconectado());
		return "Desconectado de la BD";
	}

}

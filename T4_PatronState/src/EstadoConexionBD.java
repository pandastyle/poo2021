
public abstract class EstadoConexionBD {
	
	public EstadoConexionBD() {
		
	}
	protected abstract String conectarBD(BaseDeDatos bd);
	protected abstract String desconectarBD(BaseDeDatos bd);
}

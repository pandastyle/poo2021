
public class BaseDeDatos {

	private EstadoConexionBD estado;//objeto que controla el estado interno 
	
	public BaseDeDatos() {
		this.setEstado(new EstadoConexionConectado());
	}
	
	public String conectarBD() {
		
		return this.getEstado().conectarBD(this);
	}
	
	public String desconectarBD() {
		
		return this.getEstado().desconectarBD(this);
	}

	public EstadoConexionBD getEstado() {
		return estado;
	}

	public void setEstado(EstadoConexionBD estado) {
		this.estado = estado;
	}

	
	
}

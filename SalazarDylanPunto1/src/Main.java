import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		
		Producto producto1= new Producto(1,"Microfono Blue Yeti","Microfono de alta gama",
		                "Microfono",5,10000.0);
		
		ArrayList<Producto> productos= new ArrayList<Producto>();
		productos.add(producto1);
		
		UsuarioAdministrador admin = new UsuarioAdministrador(1,"admin123","admin123",
		             "Pedro Rodriguez","pedro_20@hotmail.com","administrador",
		             productos,new ArrayList<UsuarioEmpleado>(),1);
		
		Producto producto2= new Producto(2,"Monitor 24 pulgadas","Monitor Smart TV",
		                "Pantalla",10,15000.0);
		
		System.out.println("Administrador Agrega Producto");
		System.out.println(admin.agregarProducto(producto2));
		
		
		UsuarioEmpleado empleado1 = new UsuarioEmpleado(2,"empleado1","contra123", "Pablo Ortega",
		                                "p_ortega@outlook.com","empleado",admin.getColecProductos(),"Tecnologia");
		System.out.println("Administrador Agrega Empleados");
		
		System.out.println(admin.agregarEmpleado(empleado1));
		UsuarioEmpleado empleado2 = new UsuarioEmpleado(3,"empleado2","contra124", "Juan Pepito",
		                                "pepe_123@outlook.com","empleado",admin.getColecProductos(),"Tecnologia");
		
		System.out.println(admin.agregarEmpleado(empleado2));
		
		System.out.println("-----------LISTADO EMPLEADO-------------");
		
		empleado2.listar();//lista solo productos
		
		System.out.println("-----------LISTADO Administrador-------------");
		admin.listar();//lista productos y empleados
	}

}


public class Producto {
	private Integer cod_producto;
	private String nombre_producto ;
	private String descripcion_producto;
	private String categoria;
	private Integer stock;
	private Double precio;
	
	public Producto(Integer cod_producto, String nombre_producto, String descripcion_producto, String categoria,
			Integer stock, Double precio) {
		
		this.setCod_producto(cod_producto);
		this.setNombre_producto(nombre_producto);
		this.setDescripcion_producto(descripcion_producto);
		this.setCategoria(categoria);
		this.setStock(stock);
		this.setPrecio(precio);
	}

	public Integer getCod_producto() {
		return cod_producto;
	}

	public void setCod_producto(Integer cod_producto) {
		this.cod_producto = cod_producto;
	}

	public String getNombre_producto() {
		return nombre_producto;
	}

	public void setNombre_producto(String nombre_producto) {
		this.nombre_producto = nombre_producto;
	}

	public String getDescripcion_producto() {
		return descripcion_producto;
	}

	public void setDescripcion_producto(String descripcion_producto) {
		this.descripcion_producto = descripcion_producto;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}
	
	public String mostrarInfo() {
		return "Codigo "+ getCod_producto()+ " Nombre: "+ getNombre_producto()+ 
			  " Categoria: "+getCategoria()+ " Stock: "+ getStock()+ " Precio: $"+getPrecio();
	}
	
	
}

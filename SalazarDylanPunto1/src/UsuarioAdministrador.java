import java.util.ArrayList;

public class UsuarioAdministrador extends Usuario {
	private ArrayList<UsuarioEmpleado> empleados;
	private Integer num_administrador;
	
	public UsuarioAdministrador(Integer id_usuario, String usuario, String password, String nombre_usuario,
			String correo_usuario, String tipo_usuario, ArrayList<Producto> colecProductos,
			ArrayList<UsuarioEmpleado> empleados, Integer num_administrador) {
		super(id_usuario, usuario, password, nombre_usuario, correo_usuario, tipo_usuario, colecProductos);
		
		this.setEmpleados(empleados);
		this.setNum_administrador(num_administrador);
	
		
	}
	public ArrayList<UsuarioEmpleado> getEmpleados() {
		return empleados;
	}
	public void setEmpleados(ArrayList<UsuarioEmpleado> empleados) {
		this.empleados = empleados;
	}
	public Integer getNum_administrador() {
		return num_administrador;
	}
	public void setNum_administrador(Integer num_administrador) {
		this.num_administrador = num_administrador;
	}
	
	public String agregarEmpleado(UsuarioEmpleado empleado) {
		this.getEmpleados().add(empleado);
		return "Empleado Agregado";
	}
	
	public String eliminarEmpleado(UsuarioEmpleado empleado) {
		this.getEmpleados().remove(empleado);
		return "Empleado Eliminado";
	}
	
	public void listar() {
		super.listar();
		System.out.println("Empleados del sistema");
		for (UsuarioEmpleado usuarioEmpleado : empleados) {
			System.out.println(usuarioEmpleado.infoEmpleado());
		}
	}
	
	

}

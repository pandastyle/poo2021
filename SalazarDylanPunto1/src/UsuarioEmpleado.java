import java.util.ArrayList;

public class UsuarioEmpleado extends Usuario {
	
	private String sector_empleado;
	
	public UsuarioEmpleado(Integer id_usuario, String usuario, String password, String nombre_usuario,
			String correo_usuario, String tipo_usuario, ArrayList<Producto> colecProductos,String sector_empleado) {
		super(id_usuario, usuario, password, nombre_usuario, correo_usuario, tipo_usuario, colecProductos);
		this.setSector_empleado(sector_empleado);
	}

	public String getSector_empleado() {
		return sector_empleado;
	}

	public void setSector_empleado(String sector_empleado) {
		this.sector_empleado = sector_empleado;
	}
	
	public String infoEmpleado() {
		return " Nombre de Empleado: "+ getNombre_usuario()+" Correo: "+getCorreo_usuario()+
			   " Tipo de Usuario:"+getTipo_usuario()+" Sector:"+getSector_empleado();
	}
	

}

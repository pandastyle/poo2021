import java.util.ArrayList;

public abstract class Usuario {
 private Integer id_usuario;
 private String usuario;
 private String password;
 private String nombre_usuario;
 private String correo_usuario;
 private String tipo_usuario;
 private ArrayList<Producto> colecProductos;

	 public Usuario(Integer id_usuario, String usuario, String password, String nombre_usuario, String correo_usuario,
			String tipo_usuario, ArrayList<Producto> colecProductos) {
		
		this.id_usuario = id_usuario;
		this.usuario = usuario;
		this.password = password;
		this.nombre_usuario = nombre_usuario;
		this.correo_usuario = correo_usuario;
		this.tipo_usuario = tipo_usuario;
		this.colecProductos = colecProductos;
	}

	public Integer getId_usuario() {
		return id_usuario;
	}

	public void setId_usuario(Integer id_usuario) {
		this.id_usuario = id_usuario;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNombre_usuario() {
		return nombre_usuario;
	}

	public void setNombre_usuario(String nombre_usuario) {
		this.nombre_usuario = nombre_usuario;
	}

	public String getCorreo_usuario() {
		return correo_usuario;
	}

	public void setCorreo_usuario(String correo_usuario) {
		this.correo_usuario = correo_usuario;
	}

	public String getTipo_usuario() {
		return tipo_usuario;
	}

	public void setTipo_usuario(String tipo_usuario) {
		this.tipo_usuario = tipo_usuario;
	}

	public ArrayList<Producto> getColecProductos() {
		return colecProductos;
	}

	public void setColecProductos(ArrayList<Producto> colecProductos) {
		this.colecProductos = colecProductos;
	}
	 
	public String agregarProducto(Producto prod){
		this.getColecProductos().add(prod);
		return"Producto Agregado Exitosamente";
	}
	
	public String eliminarProducto(Producto prod) {
		this.getColecProductos().remove(prod);
		return "Producto Eliminado Exitosamente";
	}
	
	public void listar() {
		
		System.out.println("Los Productos Existentes Son: ");
		for (Producto producto : colecProductos) {
			System.out.println(producto.mostrarInfo());
		}
	}
	 
 
 
 
}
